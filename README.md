# 2021_Conrad_Runser_Biased-Elongation

## Information

This repository contains source code encompanying the research paper titled "The Biomechanical Basis of Biased Epithelial Tube Elongation in Lung and Kidney Development" by Lisa Conrad, Steve Runser, Harold Gómez, Christine Lang, Mathilde Dumond, Aleksandra Sapala, Laura Kramps, Odysse Michos, Roman Vetter, and Dagmar Iber.

A preprint of the article can be found on bioRxiv at: https://doi.org/10.1101/2020.06.22.166231

## Contents

* branching-analysis: pipeline to analyse the branching of lungs and kindneys
* Chaste: source code for the cell-based tissue simulations
* COMSOL: model files for the fluid flow simulations
