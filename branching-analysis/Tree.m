% SCRIPT TO STORE TREE INFO %
%
% Mathilde Dumond
%
%
classdef Tree < handle

   properties
       time
       branchList
       nodeList
       nextTree
   end
   
   methods
       function obj = Tree(t)
         
           if nargin > 0
               obj.time = t;
               obj.branchList = [];
               obj.nextTree = 0;
               obj.nodeList = [];
           end
       end
       
       function [] = addBranch(obj, br)
           obj.branchList = [obj.branchList, br];
       end
       
       function [] = setNodeList(obj, l)
           obj.nodeList = l;
       end
       
       function [] = setBranchesTypes(obj)
           for i = 1:size(obj.branchList,2)
               obj.branchList(i).setType()
           end
       end
       
       function err = mapPoints(obj, treeBef, directory)
           err = 0;
           if treeBef.time > obj.time
               error('This function should not be called with these arguments (The times have to agree)')
           end
           listPoints = Node.empty(size(treeBef.nodeList,2),0);
           for pi = 1:size(treeBef.nodeList,2)
               [newp] = findNearestPoint(treeBef.nodeList(pi), obj.nodeList);
               listPoints(pi,1) = treeBef.nodeList(pi);
               listPoints(pi,2) = newp;
               f = find(listPoints(1:end,2)==newp);
               while size(f,1)==2
                   [listPoints] = correctDist...
                       (listPoints,f, obj.nodeList);
                   f = find(listPoints(1:end,2)==newp);
               end
           end

           check = 0;
           alreadyPr = [];
           for pi=1:size(listPoints,1)
               f = find(listPoints(1:end,2)==listPoints(pi,2));
               if (size(f,1)~=1) && not(ismember(f(1),alreadyPr))
                   check = 1;
                   alreadyPr = [alreadyPr; f];
               end
           end
           if check == 1
               comBrBef = dispCommonBr(listPoints(alreadyPr));
           end
           for pi = 1:size(listPoints,1)
               listPoints(pi,1).daughter = listPoints(pi,2);
               listPoints(pi,2).parent = listPoints(pi,1);
           end
           saveLineageNode(obj, treeBef, directory)
            if check == 1
                obj.plotTreeMap(treeBef, directory, comBrBef,1)
                err = 1;
            else
                obj.plotTreeMap(treeBef, directory)
            end
       end
       
       function err = mapBranches(obj)
           err = [];
           for br = obj.branchList
                   errT = br.setDaughter();
                   if errT ~=0
                        err = [err, br];
                   end
           end
       end
       
       function [] = setLineage(obj, treeBef, directory, allTrees, time_ori)
           err = 0;
           fprintf('Doing the lineage between times %s and %s ',...
               num2str(treeBef.time), num2str(obj.time))
           Name = strcat(directory,'/Output/NodeLineage_T',num2str(treeBef.time),...
               '-',num2str(obj.time),'.csv');
           if exist(Name, 'file') == 2
               fprintf('from a csv file\n')
               err = readLineage(obj, treeBef, directory, Name);
           else
               fprintf('from the mapping of the points\n')
               err = obj.mapPoints(treeBef, directory);
               saveLineageNode(obj, treeBef, directory)
           end
           treeBef.nextTree = obj;
           err2 = treeBef.mapBranches();
           if numel(err2) >0
               obj.plotTreeMap(treeBef, directory, err2,2)
               err = 1;
           end
           if err == 0
                treeBef.checkTermBranchTree();
           end
           if err ~=0
               quest = strcat('There was a problem when doing the lineage of times ',...
                   num2str(treeBef.time), ' and ', num2str(obj.time),...
                   '. Please open the file Plot/Lineage_T',num2str(treeBef.time),'-',...
                   num2str(obj.time),'.png and indicate whether a branch disappeared or points were mismatched.');
               quest = {quest, '','','','',''};
               ans1 = 'A branch disappeared';
               ans2 = 'Points mismatch';
               ans3 = 'Both';
               ans4 = 'I don''t know/neither';
               list = {ans1, ans2, ans3, ans4};
               answer = listdlg('PromptString',quest,'SelectionMode', 'single','ListString', list);
               if answer == 4
                   error('Correct the lineage')
               elseif (answer == 1)||(answer == 3)
                   correctDisappearBr(obj, treeBef, directory, allTrees, time_ori)
                   return
               elseif answer == 2
                   correctMisLineage(obj, treeBef, directory, allTrees, time_ori)
               end
               if obj.time ~= time_ori
                   setLineage(obj.nextTree, obj, directory, allTrees, time_ori)
               end
           end

           if (err~=0) && (answer~=2)
                    res = saveLineageNode(obj, treeBef, directory);
               if res == 0
                   setLineage(obj, treeBef, directory, allTrees, time_ori)
               end
           end
           if (obj.time)~=time_ori
               setLineage(obj.nextTree, obj, directory, allTrees, time_ori)
           end
           treeBef.updateNextTreeOrder()
       end
       
       function [] = correctMisLineage(obj, treeBef, directory, allTrees, time_ori)
           obj.plotTree(directory,'numbersNodes')
           treeBef.plotTree(directory,'numbersNodes')
           quest = strcat('Check the file Plot/Lineage_T',num2str(treeBef.time),'-',...
                   num2str(obj.time),'.png, Plot/numbersNodes_T',...
                   num2str(treeBef.time),'.png and Plot/numbersNodes_T',...
                   num2str(obj.time),'.png and correct the file NodeLineageT',...
                   num2str(treeBef.time),'-',num2str(obj.time),'. Then click OK.');
           answer = questdlg(quest, 'Correct lineage',...
                   'OK','Cancel','OK');
           t = treeBef.time;
           setLineageGen(allTrees, t, directory, time_ori)
       end
       
       function [] = correctDisappearBr(obj, treeBef, directory, allTrees, time_ori)
           quest = 'Do you already know which branch to remove?';
           answ = questdlg(quest,'Dialog Box');
           if strcmp(answ,'Yes')~=1
               treeBef.plotTree(directory,'numbersindex')
           end
           quest = strcat('Check the file Plot/Lineage_T',num2str(treeBef.time),'-',...
                   num2str(obj.time),'.png and Plot/numbersNodes_T',...
                   num2str(treeBef.time),'.png and state below which branch should be removed');
           brIndex = inputdlg(quest);
           brIndex = str2double(brIndex);
           rem = [treeBef.time,brIndex];
           updateRemove(rem, directory)
           Br = findBr(treeBef, brIndex);
           treeBef.removeBranch(Br)
           filename = strcat(directory,'/Output/NodeLineage_T', num2str(treeBef.time),...
               '-',num2str(obj.time),'.csv');
           t = treeBef.time;
           setLineageGen(allTrees, t, directory, time_ori)
       end
       
       function [] = saveLineageNode(obj, treeBef, directory)
           data = [treeBef.time, obj.time];
           filename = strcat(directory,'/Output/NodeLineage_T',num2str(treeBef.time),...
               '-',num2str(obj.time),'.csv');
           for n=treeBef.nodeList
               nd = n.daughter;
               data = [data;n.index, nd.index];
           end
           csvwrite(filename, data)
       end
       
       function err = readLineage(obj, treeBef, directory, filename)
           err = 0;
           num = csvread(filename,1,0);
           listPoints = [];
           for l = 1:size(num,1)
               n1 = treeBef.findNodeInd(num(l,1));
               n2 = obj.findNodeInd(num(l,2));
               if (n2~=0) && (n1~=0)
                   listPoints = [listPoints;n1,n2];
               elseif (n2==0) && (n1~=0)
                   [newp] = findNearestPoint(n1, obj.nodeList);
                   listPoints = [listPoints;n1,newp];
               end
           end
           check = 0;
           alreadyPr = [];
           for pi = 1:numel(treeBef.nodeList)
               f = find(listPoints(1:end,1)==treeBef.nodeList(pi));
               if numel(f)~=1
                   check = 1;
                   alreadyPr = [alreadyPr; treeBef.nodeList(pi)];
               end
           end
           for pi=1:size(listPoints,1)
               f = find(listPoints(1:end,2)==listPoints(pi,2));
               if (size(f,1)~=1) && not(ismember(listPoints(f(1)),alreadyPr))
                   check = 1;
                   alreadyPr = [alreadyPr; listPoints(f)];
               end
           end
           if check == 1
               fprintf('Check these branches of the time %s:',...
                       num2str(treeBef.time))
               comBrBef = dispCommonBr(alreadyPr);
           end
           for pi = 1:size(listPoints,1)
               listPoints(pi,1).daughter = listPoints(pi,2);
               listPoints(pi,2).parent = listPoints(pi,1);
           end
           if check == 1
               obj.plotTreeMap(treeBef, directory, comBrBef,1)
               err = 1;
               return
           else
               obj.plotTreeMap(treeBef, directory)
           end
             saveLineageNode(obj, treeBef, directory)
       end
       
       function [] = plotTree(obj,directory, what)
           fprintf('Plotting tree time %s\n', num2str(obj.time))
           fig=figure;
           if strcmp(directory, 'active') == 0
               fonts = 4;
              set(fig, 'Visible', 'off')
           else
               set(fig, 'Visible', 'on')
               fonts = 12;
           end
           t='';
           if nargin>2
               if strcmp(what,'numbersNodes')==1
                   for b=obj.branchList
                       b.plotBranch('b','index')
                       hold on
                   end
                   for n=obj.nodeList
                       xn = n.coords(1);
                       yn = n.coords(2);
                       tx = text(xn,yn, num2str(n.index),'fontsize',fonts);
                       tx.Color='r';
                   end
               elseif contains(what,'numbers')~=0
                   whatw = what(8:end);
                   for b=obj.branchList
                       b.plotBranch('b',whatw)
                       hold on
                   end
               elseif strcmp(what,'brEvent')==1
                   for b=obj.branchList
                       if strcmp(eval(strcat('b.',what)),'Terminal')==1
                           col='b';
                       elseif strcmp(eval(strcat('b.',what)),'Lateral')==1
                           col='r';
                       elseif strcmp(eval(strcat('b.',what)),'Weird')==1
                           col='g';
                       else
                           col='k';
                       end
                       b.plotBranch(col)
                       hold on
                   end
               elseif strcmp(what,'')==1
                   for b=obj.branchList
                       b.plotBranch('b')
                       hold on
                   end
               else
                   colorstring = 'kbgrymc';
                   for b=obj.branchList
                       b.plotBranch(colorstring(mod(eval(strcat('b.',what)),6)+1))
                       hold on
                   end
               end
           else
               what = '';
               for b=obj.branchList
                   b.plotBranch('k')
                   hold on
               end
           end
           if strlength(t)==0
               title(['Tree time ', num2str(obj.time),...
                   ', display ',what])
           else
               title(t)
           end
           if strcmp(directory, 'active') == 1
               hold off
           else
           print(fig,strcat(directory,'/Plot/',what,'_T',num2str(obj.time),...
               '.png'),'-dpng','-r800');
           end
       end
       
       function [] = plotTreeMap(obj, treeBef, directory, comBrBef,col)
           disp('Plotting the trees')
           fig = figure;
           set(fig, 'Visible', 'off')
           for b=obj.branchList
               if ~exist('comBrBef')
                   b.plotBranch('k')
               elseif ismember(b, comBrBef)
                   if ~exist('col')
                       b.plotBranch('g','index')
                   elseif col==1
                       b.plotBranch('g','index')
                   else
                       b.plotBranch('c','index')
                   end
               else
                   b.plotBranch('k')
               end
               hold on
               b.plotBranch('k')
               hold on
           end
           for b=treeBef.branchList
               if ~exist('comBrBef')
                   b.plotBranch('b')
               elseif ismember(b, comBrBef)
                   if ~exist('col')
                       b.plotBranch('g','index')
                   elseif col==1
                       b.plotBranch('g','index')
                   else
                       b.plotBranch('c','index')
                   end
               else
                   b.plotBranch('b')
               end
               hold on
           end
           for n=treeBef.nodeList
               if numel(n.daughter)==1
                   x = [n.coords(1), n.daughter.coords(1)];
                   y = [n.coords(2), n.daughter.coords(2)];
                   plot(x,y,'r')
                   hold on
               end
           end
           title(['Lineage of times ', num2str(treeBef.time),...
               ' and ', num2str(obj.time)])
           print(fig,strcat(directory,'/Plot/Lineage_T',num2str(treeBef.time),...
               '-',num2str(obj.time),'.png'),'-dpng','-r300');
           clear fig
       end
       
       function l = extrNodeList(obj)
           l=[];
           for n = obj.nodeList
               if size(n.branchList,2)==1
                   l = [l,n];
               end
           end
       end
       
       function [] = checkTermBranchTree(obj)
           for b=obj.branchList
               b.checkTermBranch();
           end
       end
       
       function [] = readBranchIm(obj, brname)
           im = imread(brname);
           labJ = size(obj.branchList,2)+1;
           for i=1:size(im,1)
                for j=1:size(im,2)
                    if im(i,j) == labJ
                        node = obj.findNode(i,j);
                        for b=node.branchList
                            b.addPixel([i,j])
                        end
                    elseif (im(i,j) ~= 0) && (im(i,j) ~= -1)
                        b=obj.findBr(im(i,j));
                        if not(b==0)
                            b.addPixel([i,j])
                        else
                            parulamod = [1,1,1;parula(100)];
                            image(im,'CDataMapping','scaled')
                            colormap(parulamod)
                            colorbar
                            disp([i,j])
                            disp(im(i,j))
                            disp(labJ)
                            error('I did not find the branch of this pixel')
                        end
                    end
                end
           end
       end
       
       function [] = removeBranch(obj, br)
           for n=br.nodeList
               if numel(n.branchList) == 1
                   removeBranchTerm(obj, br)
                   return
               end
           end
           removeBranchMid(obj, br)
       end
       
       function [] = removeBranchMid(obj, br)
           obj.branchList = obj.branchList(obj.branchList~=br);
           n = br.nodeList;
           obj.nodeList = obj.nodeList(obj.nodeList~=n(2));
           nmid = n(2);
           bL = nmid.branchList(nmid.branchList~=br);
           b1 = bL(1).mergeBranches(br);
           b2 = bL(2).mergeBranches(br);
           nmid.branchList = nmid.branchList(nmid.branchList~=br);
           obj.branchList = obj.branchList(obj.branchList~=nmid.branchList(1));
           obj.branchList = obj.branchList(obj.branchList~=nmid.branchList(2));
           obj.branchList = [obj.branchList, b1, b2];
       end
       
       function [] = removeBranchTerm(obj, br)
           fprintf('Removing terminal branch nb %s from time %s...\n',...
               num2str(br.index), num2str(obj.time))
           obj.branchList = obj.branchList(obj.branchList~=br);
           n = br.nodeList;
           if numel(n(1).branchList) <4
               obj.nodeList = obj.nodeList(obj.nodeList~=n(1));
           end
           if numel(n(2).branchList) <4
               obj.nodeList = obj.nodeList(obj.nodeList~=n(2));
           end
           nmid = 0;
           for nt = n
               if numel(nt.branchList) == 3
                   nmid = nt;
                   nmid.branchList = nmid.branchList(nmid.branchList~=br);
               elseif numel(nt.branchList) > 3
                   nmid = nt;
                   nmid.branchList = nmid.branchList(nmid.branchList~=br);
                   return
               end
           end
           if nmid ~= 0
               b = nmid.branchList(1).mergeBranches(nmid.branchList(2));
               obj.branchList = obj.branchList(obj.branchList~=nmid.branchList(1));
               obj.branchList = obj.branchList(obj.branchList~=nmid.branchList(2));
               obj.branchList = [obj.branchList, b];
           end
       end
       
       function [] = removeBranchSpe(obj, b)
           obj.branchList = obj.branchList(obj.branchList~=b);
           for n=b.nodeList
               n.branchList = n.branchList(n.branchList~=b);
           end
       end
       
       function newn = findNode(obj, iN,jN)
            newn = 0;
            for n = obj.nodeList
                nt = n.imageCoords();
                if isequal(nt, [iN,jN])
                    newn = n;
                end
            end
            if newn==0
                disp('No node found at the coordinates ')
                disp([iN,jN])
                for n = obj.nodeList
                    nt = n.imageCoords();
                    disp(nt)
                end
                error('No node found at the coordinates ')
            end
       end
       
       function newn = findNodeInd(obj, i)
           newn = 0;
            for n = obj.nodeList
                nt = n.index();
                if nt == i
                    newn = n;
                end
            end
       end
       
       function newb = findBr(obj, ind)
           newb=0;
           for b = obj.branchList
               if b.index == ind
                   newb = b;
               end
           end
       end
       
       function [] = readBoneJ(obj, boneJname)
           imBoneJ = imread(boneJname);
           for b=obj.branchList
               b.readboneJ(imBoneJ)
           end
       end
       
       function [] = setBrOrder(obj, directory)
           Name = [directory, '/BranchGeneration.csv'];
           if numel(obj.branchList)==1
               br = obj.branchList;
           elseif exist(Name, 'file') == 2
               obj.plotTree(directory, 'numbersindex')
               obj.setBrOrderFromFile(Name)
           else
               quest = strcat('There is no BranchGeneration.csv file. ',...
                   'If you don''t want to use one, click CANCEL, ',...
                   'otherwise, create it, Then, click OK.');
               answer = questdlg(quest, 'BranchGeneration',...
                   'OK','Cancel','OK');
               if strcmp(answer,'OK')==1
                   obj.plotTree(directory, 'numbersindex')
                   obj.setBrOrderFromFile(Name)
               else
                   text = 'What is the number of the trunk branch?';
                   br = obj.askForBranch(text, directory);
                   br.expandRootOrder(obj.branchList)
               end
           end
           for b=obj.branchList
               b.firstTime = obj.time;
           end
           currT = obj;
       end
       
       function [] = setBrOrderFromFile(obj, filename)
           fid = fopen(filename);
           header = fgetl(fid);
           while true
               line = fgetl(fid);
               if ~ischar(line); break; end
               line = strsplit(line,',','CollapseDelimiters', false);
               br = obj.findBr(str2double(line(1)));
               if br~=0
                    br.order = str2double(line(2));
               end
           end
       end
       
       function [] = updateNextTreeOrder(obj)
           for br=obj.branchList
                if numel(br.daughterList)==1
                   br.daughterList.order = br.order;
                   br.daughterList.firstTime = br.firstTime;
                else
                   for bd=br.daughterList
                       bd.order = br.order;
                       bd.firstTime = br.firstTime;
                   end
                end
           end
           for br = obj.nextTree.branchList
               if br.firstTime == 0
                   br.firstTime = obj.time+1;
               end
               if br.order == 0
                   bN = br.getNeigh();
                   newo = 10000;
                   for bNn=bN
                       if bNn.order ~= 0
                           newo = min(newo, bNn.order);
                       end
                   end
                   if not(newo==10000)
                       br.order = newo+1;
                   else
                       disp(br.time)
                       disp(br.index)
                       disp('parent')
                       disp(br.parent)
                       disp('neighbors:')
                       for bNN=bN
                           disp(bNN.index)
                       end
                       disp('end neighbors')
                       disp('Check whether many branches are created at the same time such that a new branch is connected to only new branches. If possible, remove it.')
                       error('This branch has no order?')
                   end
               end
           end
       end
       
       function br = askForBranch(obj, text, directory)
           obj.plotTree(directory, 'numbersindex')
           b = inputdlg(text);
           br = obj.findBr(str2double(b{1}));
       end
       
       function [] = checkDaught(obj)
           for b=obj.branchList
               b.hasdaughter = numel(b.daughterList);
           end
       end
       
       function [] = setBrWidth(obj)
           for b=obj.branchList
               b.width = mean(b.allwidth);
               b.lengthovwidth = b.length/b.width;
           end
       end
       
       function [] = cleanSubTrees(obj)
           subT = [];
           for b=obj.branchList
               if b.subTree<=numel(subT)
                   subT(b.subTree) = subT(b.subTree)+1;
               else
                   subT(b.subTree) = 1;
               end
           end
           if numel(subT)>1
               newBrL = [];
               newNoL = [];
               [~,i] = max(subT);
               for b=obj.branchList
                   if b.subTree == i
                       newBrL = [newBrL, b];
                       newNoL = [newNoL, b.nodeList(1), b.nodeList(2)];
                   end
               end
               obj.branchList = newBrL;
           obj.nodeList = unique(newNoL);
           end
       end
       
       function [] = checkNodesTwoBr(obj)
           for n=obj.nodeList
               if numel(n.branchList)==2
                   b = n.branchList;
                   fprintf('One node has two branches - I am removing it and merging branches %s and %s',...
                       num2str(b(1).index), num2str(b(2).index))
                   newb = mergeBranches(n.branchList(1), n.branchList(2));
                   obj.branchList = obj.branchList(obj.branchList~=n.branchList(1));
                   obj.branchList = obj.branchList(obj.branchList~=n.branchList(2));
                   obj.branchList = [obj.branchList, newb];
                   obj.nodeList = obj.nodeList(obj.nodeList~=n);
               end
           end
       end
       
       function [] = checkSmallBr(obj)
           res = 0;
           while res == 0
               temp = 0;
               for b = obj.branchList
                   b.setType()
                   if b.Elength == 0
                       if (b.nodeList(1) == b.nodeList(2)) && ...
                               (numel(unique(b.nodeList(1).branchList)) == 2)
                           obj.removeBranchSpe(b)
                       else
                           obj.removeBranchTerm(b)
                       end
                       temp = 1;
                       break
                   end
                   if (b.length < 10) && (b.type==0)
                       obj.removeBranch(b)
                       temp = 1;
                       break
                   end
               end
               if temp == 0
                    res = 1;
               end
           end
       end
       
       function [] = defineBranchSectors(obj)
           br = obj.findRoot();
           br.sector = 1;
           n = higherNode(br.nodeList(1), br.nodeList(2));
           toDoList = n.branchList;
           doneList = br;
           while numel(toDoList) ~=0
               b = toDoList(1);
               toDoList(1)=[];
               doneList = [doneList, b];
               newBr = b.getNeigh();
               for br=newBr
                    if not(ismember(br, doneList))
                        if not(ismember(br, toDoList))
                            toDoList = [toDoList, br];
                        end
                    elseif br.order < b.order
                        b.sector = 1;
                    elseif (br.order == b.order) && (numel(b.sector)==0)
                        b.sector = br.sector+1;
                    end
               end
           end
       end
       
       function [] = defineBranchOrientation(obj)
           for b=obj.branchList
               if b.order ~=1
                   b.defineOrientation()
               end
           end
       end
       function br = findRoot(obj)
           for b=obj.branchList
               if b.order == 1
                   br = b;
               end
           end
       end
       
       function A = getAdjMatrix(obj)
           A = zeros(size(obj.nodeList));
           for b = obj.branchList
               n = b.nodeList();
               n1 = n(1).index;
               n2 = n(2).index;
               A(n1, n2) = 1;
               A(n2, n1) = 1;
           end
       end
       
       function G = translateGraph(obj)
           A = getAdjMatrix(obj);
           G = graph(A);
       end
       
       function score = compareTrees(obj, tree)
           if size(obj.branchList) == size(tree.branchList)
               G1 = translateGraph(obj);
               G2 = translateGraph(tree);
               if isisomorphic(G1, G2)
                   score = 0;
               else
                   score = 200;
               end
           else
               score = 100*(abs(size(obj.branchList,2)-size(tree.branchList,2)));
           end
               
       end
   end
end

function [] = updateRemove(rem, directory)
    filename = strcat(directory,'/Output/Remove.csv');
    try
        num = csvread(filename,1,0);
    catch
        num = [];
    end
    num = [num;rem];
    numw = array2table(num,'VariableNames',{'Time_File','Branch'});
    writetable(numw,filename)
end

function [] = setLineageGen(allTrees, time, directory, time_ori)
    if time~=allTrees(1).time
        tree = findTree(allTrees, time);
        treeBef = findTree(allTrees, time, 'prev');
    else
        tree = findTree(allTrees, time,'aft');
        treeBef = findTree(allTrees, time);
    end
    setLineage(tree, treeBef, directory, allTrees, time_ori)
end

function [listPoints] = correctDist(listPoints,f, nodeList)
    d1 = listPoints(f(1),1).dist(listPoints(f(1),2));
    d2 = listPoints(f(2),1).dist(listPoints(f(2),2));
    if d1 < d2
        ptochange = 2;
        pstill = 1;
    else
        ptochange = 1;
        pstill = 2;
    end
    newList = nodeList(nodeList~=listPoints(f(pstill),2));
    [newn] = listPoints(f(ptochange),1).findNearestPoint(newList);
    listPoints(f(ptochange),2) = newn;
end

function [brF] = dispCommonBr(listPoints)
    brL = [];
    brF = [];
    for n=1:size(listPoints)
        nBrL = [];
        if numel(listPoints(n).branchList)>1
            for b=listPoints(n).branchList
                nBrL = [nBrL, b];
            end
        else
            nBrL = listPoints(n).branchList;
        end
        for b=nBrL
            if ismember(b, brL)
                brF = [brF, b];
            else
                brL = [brL, b];
            end
        end
    end
    if numel(brF)==0
        brF = brL;
    end
end

function tr = findTree(allTrees, treetime, optArg)
    for ti=1:numel(allTrees)
        if allTrees(ti).time == treetime
            if nargin == 2
                tr = allTrees(ti);
                return
            elseif strcmp(optArg,'aft')
                tr = allTrees(ti+1);
            elseif strcmp(optArg,'prev')
                tr = allTrees(ti-1);
            end
        end
    end
    fprintf('I did not find a tree for the time %s', num2str(treetime))
end