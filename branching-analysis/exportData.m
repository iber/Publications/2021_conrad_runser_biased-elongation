% SCRIPT TO EXPORT DATA %
%
% Mathilde Dumond
%
%
function exportData(trees, directory)
    exp = strsplit(directory,'/');
    path = strjoin(exp(1:end-1),'/');
    exp = exp{end};
    mkdir(path,'00_SUMMARIES');
    fileName = strcat(path,'/00_SUMMARIES/',exp,'.csv');
    listExt = {'time','index','length','width','coordsEnds','order','sector','orientation','type','brEvent','parentId','firstTime','angle1','angle2','angle3','angle4','angle1Ref','angle2Ref','angle3Ref','angle4Ref'};
    listExtF = {'time','index','length','width','End1x','End1y','End1z','End2x','End2y','End2z','order','sector','orientation','type','brEvent','parentId','firstTime','angle1','angle2','angle3','angle4','angle1Ref','angle2Ref','angle3Ref','angle4Ref'};
    allExt = {};

    for t=trees
        for b = t.branchList
            ext = b.extractData(listExt);
            allExt{end+1} = ext;
        end
    end
    
    allExt2 = vertcat(allExt{:});
    fid = fopen(fileName, 'w');
    fprintf(fid,[repmat('%s, ',1,numel(listExtF)-1),'%s\n'],listExtF{:});
    
    for i=1:size(allExt2,1)
        fprintf(fid,[repmat('%s, ',1,numel(listExtF)-1),'%s\n'],allExt2{i,:});
    end
    
    fclose(fid);
end