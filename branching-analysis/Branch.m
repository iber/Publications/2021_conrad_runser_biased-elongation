% SCRIPT TO STORE BRANCH INFO %
%
% Mathilde Dumond
%
%
classdef Branch < handle
    
    properties
        index
        time
        subTree
        nodeList
        length
        Elength
        width
        allwidth
        lengthovwidth
        parent
        undirParent
        daughterList
        undirDaughterList
        pixelList
        type
        order
        sector
        orientation
        firstTime
        remove
        hasdaughter
        brEvent
        angle1
        angle2
        angle3
        angle4
        angle1Ref
        angle2Ref
        angle3Ref
        angle4Ref
    end
    
    methods
        function obj = Branch(i)
            if nargin > 0
                obj.index = i;
                obj.nodeList = [];
                obj.order = 0;
                obj.firstTime = 0;
                obj.brEvent = '';
                obj.daughterList=[];
                obj.undirDaughterList=[];
            end
        end
        
        function [] = addNode(obj, n)
            obj.nodeList = [obj.nodeList, n];
        end
        
        function [] = setLength(obj, l)
            obj.length = l;
        end
        
        function [] = setElength(obj,l)
            obj.Elength = l;
        end
        
        function [lBr] = getNeigh(obj)
            for n=1:size(obj.nodeList,2)
                lBr = [obj.nodeList(1).branchList(), obj.nodeList(2).branchList()];
            end
        end
        
        function [] = setType(obj)
            obj.type = 1;
            for i=1:numel(obj.nodeList)
                if numel(obj.nodeList(i).branchList) == 1
                    obj.type = 0;
                end
            end
        end
        
        function [] = setUndirLineage(obj)
            for d = obj.daughterList
                for dd = d.getNeigh()
                    if numel(dd.parent)==0
                        dd.undirParent = obj;
                        obj.undirDaughterList = [obj.undirDaughterList, dd];
                    end
                end
            end
            if (numel(obj.parent)==0) && (numel(obj.undirParent)==0)
                for dd = obj.getNeigh()
                    if numel(dd.parent)~=0
                        obj.undirParent = dd.parent;
                    elseif numel(dd.undirParent)~=0
                        obj.undirParent = dd.undirParent;
                    end
                end
            end
        end
        
        function [] = expandRootOrder(obj, listBr)
            tempListBr = listBr(listBr~=obj);
            obj.order = 1;
            while numel(tempListBr)~=0
                currB = tempListBr(1);
                if currB.order ~=0
                    tempListBr = tempListBr(tempListBr~=currB);
                else
                    newo = currB.findOrder();
                    if newo ~=0
                        currB.order = newo;
                        tempListBr = tempListBr(tempListBr~=currB);
                    else
                        tempListBr = [tempListBr(2:end),tempListBr(1)];
                    end
                end
            end
        end
        
        function ord = findOrder(obj)
            lBr = obj.getNeigh();
            ord = 1000;
            for b=lBr
                if b.order~=0
                    ord = min(ord,b.order);
                end
            end
            if ord==1000
                ord = 0;
            else
                ord = ord+1;
            end
        end
        
        function err = setDaughter(obj)
            err = 0;
            nodesBef = obj.nodeList;
            nodesAft = [nodesBef(1).daughter, nodesBef(2).daughter];
            brAft1 = nodesAft(1).branchList;
            brAft2 = nodesAft(2).branchList;
            B = intersect(brAft1, brAft2);
            if size(B,2)==1
                obj.daughterList = B;
                B.parent = obj;
            else
                fprintf('There is a branching event here\n The branch %s gives the branches ',...
                    num2str(obj.index))
                bL = findThe2Branches(nodesAft);
                if bL==0
                    err = 1;
                    return
                end
                obj.daughterList = bL;
                if size(obj.daughterList,2)==2
                    termBr = 0;
                    obj.determineBranchingEvent(0, termBr, nodesAft)
                    for b=obj.daughterList
                        fprintf('%s, ',num2str(b.index))
                        if b.type == 0
                            termBr = b;
                            midBr = obj.daughterList(obj.daughterList~=b);
                        end
                    end
                    fprintf('\n')
                    if termBr ~=0
                        errb = obj.determineBranchingEvent(midBr, termBr, nodesAft);
                        if errb ~= 0
                            err = 2;
                        end
                    end
                else
                    termBr = 0;
                    for b=obj.daughterList
                        if numel(b.parent)==0
                            b.parent = obj;
                        elseif (b.parent ~= obj)
                            disp('##Trying to set a parent to a branch with already a parent##')
                            disp(obj.time)
                            disp(obj.index)
                            disp(b.time)
                            disp(b.index)
                            disp(b.parent)
                            err = 2;
                        end
                        if (b.type == 0) && (sum(ismember(b.nodeList, nodesAft))==1)
                            termBr = b;
                            notb = obj.daughterList(obj.daughterList~=b);
                            midBr = notb(1).addBranches(notb(2));
                        end
                    end
                    if termBr ~=0
                        res = obj.determineBranchingEvent(midBr, termBr, nodesAft);
                    end
                end
            end
            for d=obj.daughterList
                if numel(d.parent)==0
                    d.parent = obj;
                elseif (d.parent ~= obj)
                    temp = d.parent;
                    if obj.order<temp.order
                        d.parent = obj;
                        temp.daughterList = temp.daughterList(temp.daughterList~=d);
                    else
                        obj.daughterList = obj.daughterList(obj.daughterList~=d);
                    end
                end
            end
        end
        
        function err = determineBranchingEvent(obj, midBr, termBr, nodesAft)
            if obj.type == 0
                obj.brEvent = 'Terminal';
            else
                obj.brEvent = 'Lateral';
            end
            err = 0;
        end
        
        function [] = determineBranchingEventOri(obj, midBr, termBr, nodesAft)
            nmid = intersect(obj.daughterList(1).nodeList,...
                obj.daughterList(2).nodeList);
            if (abs((termBr.length + midBr.length - obj.length)/obj.length) < 0.15) &&...
                    (abs((termBr.length - midBr.length)/midBr.length) < 0.3)
                obj.brEvent = 'Lateral';
            elseif termBr.length/(termBr.length+midBr.length) < 0.25
                obj.daughterList = midBr;
                obj.brEvent = 'Terminal';
                nAftch = 0;
                for n=nodesAft
                    if ismember(termBr, n.branchList)
                        nAftch = n;
                    end
                end
                if nAftch==0
                    error('see function setDaughter')
                end
                nBefch = nAftch.parent;
                nBefch.daughter = nmid;
                nAftch.parent = 0;
                nmid.parent = nBefch;
            else
                obj.brEvent = 'Weird';
            end
        end
        
        function [] = plotBranch(obj,col,numb)
            x = [obj.nodeList(1).coords(1), obj.nodeList(2).coords(1)];
            y = [obj.nodeList(1).coords(2), obj.nodeList(2).coords(2)];
            plot(x,y, col)
            hold on
            if nargin>2
                xn = mean(x);
                yn = mean(y);
                s = eval(strcat('obj.',numb));
                if isnumeric(s)
                    s = num2str(s);
                end
                tx = text(xn,yn, s);
                tx.Color='k';
            end
        end
        
        function [] = defineOrientation(obj)
            obj.angle1 = [];
            obj.angle2 = [];
            obj.angle3 = [];
            obj.angle4 = [];
            obj.angle1Ref = [];
            obj.angle2Ref = [];
            obj.angle3Ref = [];
            obj.angle4Ref = [];

            if numel(obj.orientation) == 0
                n0 = [];
                bM = [];
                for n = obj.nodeList
                    for br = n.branchList(n.branchList~=obj)
                        if (br.order < obj.order) && (numel(n0)==0)
                            n0 = n;
                            bM = br;
                        elseif (br.order < obj.order) && (br.sector<bM.sector)
                            n0 = n;
                            bM = br;
                        elseif (br.order == obj.order) && (br.sector<obj.sector) && (numel(n0)==0)
                            n0 = n;
                            bM = br;
                        end
                    end
                end
                bS = n0.branchList(n0.branchList~=bM);
                bS = bS(bS~=obj);
                if numel(bS)==1
                    if numel(bS.orientation)~=0
                        disp('error:')
                        disp([bS.time, bS.index])
                        disp(bS.orientation)
                        disp(bM.index)
                        disp(bM.orientation)
                        error('Trying to determine the orientation of a branch already determined1')
                    end
                    nM = bM.nodeList(bM.nodeList~=n0);
                    nS = bS.nodeList(bS.nodeList~=n0);
                    n = obj.nodeList(obj.nodeList~=n0);
                    A = [(nM.coords(1:2)-n0.coords(1:2))',(n.coords(1:2)-n0.coords(1:2))'];
                    B = [(nM.coords(1:2)-n0.coords(1:2))',(nS.coords(1:2)-n0.coords(1:2))'];
                    C = [(n.coords(1:2)-n0.coords(1:2))',(nS.coords(1:2)-n0.coords(1:2))'];
                      
                    nMbr = nM.branchList(ismember(nM.branchList,n0.branchList));
                    nSbr = nS.branchList(ismember(nS.branchList,n0.branchList));
                    nbr = n.branchList(ismember(n.branchList,n0.branchList));
                                                                
                    obj.angle1Ref = [nMbr.index nbr.index];
                    obj.angle2Ref = [nMbr.index nSbr.index];
                    obj.angle3Ref = [nbr.index nSbr.index];
                    obj.angle4Ref = [];
                    
                    zA = A(1,1)*A(1,2) + A(2,1)*A(2,2);
                    n1A = norm(A(:,1));
                    n2A = norm(A(:,2));
                    cosA = zA/(n1A*n2A);
                    dA = acosd(cosA);
                    obj.angle1 = dA;

                    zB = B(1,1)*B(1,2) + B(2,1)*B(2,2);
                    n1B = norm(B(:,1));
                    n2B = norm(B(:,2));
                    cosB = zB/(n1B*n2B);
                    dB = acosd(cosB);
                    obj.angle2 = dB;
                    
                    zC = C(1,1)*C(1,2) + C(2,1)*C(2,2);
                    n1C = norm(C(:,1));
                    n2C = norm(C(:,2));
                    cosC = zC/(n1C*n2C);
                    dC = acosd(cosC);
                    obj.angle3 = dC;  
                    
                    obj.angle4 = [];

                    if (det(A)<=0) && (det(B)>=0)
                        obj.orientation = 'l';
                        bS.orientation = 'r';
                    elseif (det(A)>=0) && (det(B)<=0)
                        obj.orientation = 'r';
                        bS.orientation = 'l';
                    else
                        C = [(n.coords(1:2)-n0.coords(1:2))',(nS.coords(1:2)-n0.coords(1:2))'];
                        if det(C)<=0
                            obj.orientation = 'r';
                            bS.orientation = 'l';
                        else
                            obj.orientation = 'l';
                            bS.orientation = 'r';
                        end
                    end
                                       
                elseif numel(bS)>1
                    bS1 = bS(1);
                    bS2 = bS(2);
                    nS1 = bS1.nodeList(bS1.nodeList~=n0);
                    nS2 = bS2.nodeList(bS2.nodeList~=n0);                   
                    if (numel(bS1.orientation)~=0) || (numel(bS2.orientation)~=0)
                        disp([bS1.time, bS1.index, bS2.index])
                        disp([bS1.orientation, bS2.orientation])
                        error('Trying to determine the orientation of a branch already determined2')
                    end
                    n = obj.nodeList(obj.nodeList~=n0);
                    nM = bM.nodeList(bM.nodeList~=n0);
                    A = [(nM.coords(1:2)-n0.coords(1:2))',(n.coords(1:2)-n0.coords(1:2))'];
                    B = [(nM.coords(1:2)-n0.coords(1:2))',(nS1.coords(1:2)-n0.coords(1:2))'];
                    C = [(nM.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];                    
                    C2 = [(n.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                    D = [(nS1.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                    
                    nMbr = nM.branchList(ismember(nM.branchList,n0.branchList));                    
                    nS1br = nS1.branchList(ismember(nS1.branchList,n0.branchList));
                    nS2br = nS2.branchList(ismember(nS2.branchList,n0.branchList));                    
                    nbr = n.branchList(ismember(n.branchList,n0.branchList));

                    obj.angle1Ref = [nMbr.index nbr.index];
                    obj.angle2Ref = [nMbr.index nS1br.index];
                    obj.angle3Ref = [nbr.index nS2br.index];
                    obj.angle4Ref = [nS1br.index nS2br.index];
                     
                    zA = A(1,1)*A(1,2) + A(2,1)*A(2,2);
                    n1A = norm(A(:,1));
                    n2A = norm(A(:,2));
                    cosA = zA/(n1A*n2A);
                    dA = acosd(cosA);
                    obj.angle1 = dA;

                    zB = B(1,1)*B(1,2) + B(2,1)*B(2,2);
                    n1B = norm(B(:,1));
                    n2B = norm(B(:,2));
                    cosB = zB/(n1B*n2B);
                    dB = acosd(cosB);
                    obj.angle2 = dB;
                    
                    zC2 = C2(1,1)*C2(1,2) + C2(2,1)*C2(2,2);
                    n1C2 = norm(C2(:,1));
                    n2C2 = norm(C2(:,2));
                    cosC2 = zC2/(n1C2*n2C2);
                    dC2 = acosd(cosC2);
                    obj.angle3 = dC2;  
                    
                    zD = D(1,1)*D(1,2) + D(2,1)*D(2,2);
                    n1D = norm(D(:,1));
                    n2D = norm(D(:,2));
                    cosD = zD/(n1D*n2D);
                    dD = acosd(cosD);
                    obj.angle4 = dD;
                    
                    if dA+dB+dC2+dD>360.1
                        B = [(nM.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                        C2 = [(n.coords(1:2)-n0.coords(1:2))',(nS1.coords(1:2)-n0.coords(1:2))'];                   

                        obj.angle2Ref = [nMbr.index nS2br.index];
                        obj.angle3Ref = [nbr.index nS1br.index];

                        zB = B(1,1)*B(1,2) + B(2,1)*B(2,2);
                        n1B = norm(B(:,1));
                        n2B = norm(B(:,2));
                        cosB = zB/(n1B*n2B);
                        dB = acosd(cosB);
                        obj.angle2 = dB;                  
                    
                        zC2 = C2(1,1)*C2(1,2) + C2(2,1)*C2(2,2);
                        n1C2 = norm(C2(:,1));
                        n2C2 = norm(C2(:,2));
                        cosC2 = zC2/(n1C2*n2C2);
                        dC2 = acosd(cosC2);
                        obj.angle3 = dC2; 
                        
                        if dA+dB+dC2+dD>360.1
                           A = [(nM.coords(1:2)-n0.coords(1:2))',(nS1.coords(1:2)-n0.coords(1:2))'];
                           D = [(n.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                            
                           obj.angle1Ref = [nMbr.index nS1br.index];
                           obj.angle4Ref = [nbr.index nS2br.index];
                           
                           zA = A(1,1)*A(1,2) + A(2,1)*A(2,2);
                           n1A = norm(A(:,1));
                           n2A = norm(A(:,2));
                           cosA = zA/(n1A*n2A);
                           dA = acosd(cosA);
                           obj.angle1 = dA;  

                           zD = D(1,1)*D(1,2) + D(2,1)*D(2,2);
                           n1D = norm(D(:,1));
                           n2D = norm(D(:,2));
                           cosD = zD/(n1D*n2D);
                           dD = acosd(cosD);
                           obj.angle4 = dD;
                           
                            if dA+dB+dC2+dD>360.1
                                error('The sum of the angles is bigger than 360 degrees: dA+dB+dC2+dD>360.1')
                            end
                        end
                    end                                      
                    
                    if det(A)*det(B)<=0
                        if det(A)*det(C)<=0
                            D = [(nS1.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                            if det(B)*det(C)<0
                                error('Is this configuration possible? what does it mean?')
                            elseif det(A)<=0
                                obj.orientation = 'l';
                                if det(D)>=0
                                    bS1.orientation = 'r';
                                    bS2.orientation = 'c';
                                else
                                    bS1.orientation = 'c';
                                    bS2.orientation = 'r';
                                end
                            else
                                obj.orientation = 'r';
                                if det(D)>=0
                                    bS1.orientation = 'c';
                                    bS2.orientation = 'l';
                                else
                                    bS1.orientation = 'l';
                                	bS2.orientation = 'c';
                                end
                            end
                        elseif det(A)*det(C)>=0
                            D = [(n.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                            if det(B)*det(C)>0
                                error('Is this configuration possible? what does it mean?')
                            elseif det(B)<=0
                                bS1.orientation = 'l';
                                if det(D)<=0
                                    obj.orientation = 'c';
                                    bS2.orientation = 'r';
                                else
                                    obj.orientation = 'r';
                                    bS2.orientation = 'c';
                                end
                            else
                                bS1.orientation = 'r';
                                if det(D)<=0
                                    obj.orientation = 'l';
                                    bS2.orientation = 'c';
                                else
                                    obj.orientation = 'c';
                                    bS2.orientation = 'l';
                                end
                            end
                        end
                        
                        
                        
                    elseif det(A)*det(B)>=0
                        if det(A)*det(C)<=0
                            D = [(n.coords(1:2)-n0.coords(1:2))',(nS1.coords(1:2)-n0.coords(1:2))'];
                            if det(B)*det(C)>0
                                error('Is this configuration possible? what does it mean?')
                            elseif det(C)<=0
                                bS2.orientation = 'l';
                                if det(D)<=0
                                    obj.orientation = 'c';
                                    bS1.orientation = 'r';
                                else
                                    obj.orientation = 'r';
                                    bS1.orientation = 'c';
                                end
                            else
                                bS2.orientation = 'r';
                                if det(D)<=0
                                    obj.orientation = 'l';
                                    bS1.orientation = 'c';
                                else
                                    obj.orientation = 'c';
                                    bS1.orientation = 'l';
                                end
                            end
                        elseif det(A)*det(C)>=0
                            D = [(n.coords(1:2)-n0.coords(1:2))',(nS1.coords(1:2)-n0.coords(1:2))'];
                            E = [(n.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                            F = [(nS1.coords(1:2)-n0.coords(1:2))',(nS2.coords(1:2)-n0.coords(1:2))'];
                            if (det(D)>=0) && (det(E)>=0)
                                obj.orientation = 'l';
                                if det(F)>=0
                                    bS1.orientation = 'r';
                                    bS2.orientation = 'c';
                                else
                                    bS1.orientation = 'c';
                                    bS2.orientation = 'r';
                                end
                            elseif (det(D)<=0) && (det(F)>=0)
                                bS1.orientation = 'l';
                                if det(E)>=0
                                    obj.orientation = 'r';
                                    bS2.orientation = 'c';
                                else
                                    obj.orientation = 'c';
                                    bS2.orientation = 'r';
                                end
                            elseif (det(E)<0) && (det(F)<0)
                                bS2.orientation = 'l';
                                if det(D)>0
                                    obj.orientation = 'r';
                                    bS1.orientation = 'c';
                                else
                                    obj.orientation = 'c';
                                    bS1.orientation = 'r';
                                end
                            end
                        end
                    end
                else
                    for b=n0.branchList
                        disp(b.index)
                    end
                    error('a branching event with only 2 branches?')
                end
            end
            if numel(obj.orientation)==0
                error('the orientation has not been determined')
            end
        end
        
        function [] = addPixel(obj, coord)
            obj.pixelList = [obj.pixelList; coord];
        end
        
        function newb = mergeBranches(obj, br)
            fprintf('Merging the branches %s and %s \n',...
                num2str(obj.index), num2str(br.index))
            bL = [];
            for n = obj.nodeList
                bL = [bL, n.branchList];
            end
            if not(ismember(br, bL))
                br.plotBranch('b')
                hold on
                for bl=bL
                    bl.plotBranch('k')
                    hold on
                end
                hold off
                error('Trying to merge 2 branches that are not neighbors')
            end
            if isstruct(obj.index)
                disp(obj.index)
                error('structure index')
            end
            newb = Branch(obj.index);
            newb.time = obj.time;
            newnodes = [];
            for n=obj.nodeList
                if not(ismember(n,br.nodeList))
                    newnodes = [newnodes, n];
                end
            end
            for n=br.nodeList
                if not(ismember(n,obj.nodeList))
                    newnodes = [newnodes, n];
                end
            end
            newb.nodeList = newnodes;
            for n=newnodes
                n.branchList = n.branchList(n.branchList~=obj);
                n.branchList = n.branchList(n.branchList~=br);
                n.branchList = [n.branchList, newb];
            end
            newb.length = br.length+obj.length;
            newb.allwidth = [br.allwidth, obj.allwidth];
            newb.subTree = br.subTree;
            newb.daughterList = [br.daughterList, obj.daughterList];
            if obj.parent ~= br.parent
                disp([obj.index, obj.parent.index; br.index, br.parent.index])
                disp('Warning: the 2 branches you want to merge do not have the same parent.')
            end
            newb.parent = obj.parent;
            newb.pixelList = [obj.pixelList; br.pixelList];
            newb.setType()
        end
        
        function newb = addBranches(obj, br)
            fprintf('Merging the branches %s and %s \n',...
                num2str(obj.index), num2str(br.index))
            bL = [];
            for n = obj.nodeList
                bL = [bL, n.branchList];
            end
            if not(ismember(br, bL))
                br.plotBranch('b')
                hold on
                for bl=bL
                    bl.plotBranch('k')
                    hold on
                end
                hold off
                error('trying to merge 2 branches that are not neighbors')
            end
            if isstruct(obj.index)
                disp(obj.index)
                error('structure index')
            end
            newb = Branch(obj.index);
            newnodes = [];
            for n=obj.nodeList
                if not(ismember(n,br.nodeList))
                    newnodes = [newnodes, n];
                end
            end
            for n=br.nodeList
                if not(ismember(n,obj.nodeList))
                    newnodes = [newnodes, n];
                end
            end
            newb.nodeList = newnodes;
            newb.length = br.length+obj.length;
            newb.daughterList = [br.daughterList, obj.daughterList];
            if obj.parent ~= br.parent
                disp('Warning: the 2 branches you want to merge do not have the same parent.')
            end
            newb.parent = obj.parent;
            newb.pixelList = [obj.pixelList; br.pixelList];
            newb.setType()
        end
        
        function [] = readboneJ(obj, imBoneJ)
            allwid = zeros(1,0);
            for p=1:size(obj.pixelList,1)
                allwid(p) = imBoneJ(obj.pixelList(p,1),obj.pixelList(p,2));
            end
            obj.allwidth = allwid;
        end
        
        function [] = checkTermBranch(obj)
            if strcmp(obj.brEvent,'') && (obj.type == 0) && (obj.daughterList.type == 1)
                obj.brEvent = 'Terminal';
            end
        end
        
        function res = isSingleDaught(obj)
            p = obj.parent;
            nbSib = numel(p.daughterList);
            if nbSib == 1
                res= 1;
            else
                res = 0;
            end
        end
        
        function dat = extractData(obj, listExt)
            dat = {};
            for e = 1:numel(listExt)
                if strcmp(listExt{e},'coordsEnds')==1
                    add = obj.getCoordsEnds();
                elseif strcmp(listExt{e}, 'parentId')==1
                    add = obj.getParentId();
                else
                    add = {num2str(obj.(listExt{e}))};
                end
                for i=1:numel(add)
                    dat{end + 1} = add{i};
                end
            end
        end
        
        function ans = getCoordsEnds(obj)
            ans = {};
            for n=obj.nodeList
                for c=n.coords
                    ans{end+1}=num2str(c);
                end
            end
        end
        function ans = getParentId(obj)
            if numel(obj.undirParent) ~= 0
                p = obj.undirParent;
                ans = {num2str(p.index)};
            elseif numel(obj.parent) ~= 0
                p = obj.parent;
                ans = {num2str(p.index)};
            else
                ans = {'0'};
            end
        end
    end
end

function bL = findThe2Branches(nodesAft)
    brAft1 = nodesAft(1).branchList;
    for b=brAft1
        n2 = 0;
        for n = b.nodeList
            if n~=nodesAft(1)
                n2 = n;
            end
        end
        for b2 = n2.branchList
            if ismember(nodesAft(2), b2.nodeList)
                bL = [b,b2];
                return
            end
        end
    end
    for b=brAft1
        n2 = 0;
        for n = b.nodeList
            if n~=nodesAft(1)
                n2 = n;
            end
        end
        for b2 = n2.branchList
            n3 = 0;
            for n = b2.nodeList
                if (n~=n2) && (n~=nodesAft(1))
                    n3 = n;
                end
            end
            if (n3~=0) && (numel(n3.branchList) >1)
                for b3 = n3.branchList
                    if ismember(nodesAft(2), b3.nodeList)
                        bL = [b,b2,b3];
                        return
                    end
                end
            elseif (n3~=0)
                b3 = n3.branchList;
                if ismember(nodesAft(2), b2.nodeList)
                    bL = [b,b2,b3];
                    return
                end
            end
        end
        bL = 0;
    end
end