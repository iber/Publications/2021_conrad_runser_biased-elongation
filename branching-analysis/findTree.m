% SCRIPT TO FIND TREES %
%
% Mathilde Dumond
%
%
function tr = findTree(allTrees, treetime)
for t=allTrees
    if t.time == treetime
        tr = t;
        return
    end
end
fprintf('I did not find a tree for the time %s', num2str(treetime))
end