% SCRIPT TO REMOVE BRANCHES %
%
% Mathilde Dumond
%
%
function tree = checkRemoveSingle(removeFile, tree)
% Reads the Remove.csv file and removes all branches that are specified there
try
    num = csvread(removeFile,1,0);
catch
    disp('No Remove.csv file found')
    return
end
for l = 1:size(num,1)
    line = num(l,:);
    if (line(2)~=0) && (line(1)==tree.time)
        treetime = line(1);
        removeBrInd = line(2);
        fprintf('Removing branch %s from tree time %s \n', ...
            num2str(removeBrInd), num2str(treetime))
        br = tree.findBr(removeBrInd);
        tree.removeBranch(br)
    end
end
end