Branching Analysis
========

*Authors*: Mathilde Dumond, Christine Lang, Lisa Conrad

*Contributions*: MD wrote the main MATLAB script. CL extended the MATLAB script for additional analysis purposes. LC and CL established the Fiji pipeline for image processing.  


Table of Contents
-----------------
[[_TOC_]]

Introduction
-----------------------------
During organogenesis, the coordination of growth and
patterning processes is essential for the formation of
a functional organ. Branching morphogenesis occurs
as a common motif in the development of the lungs,
kidneys and many glands as their function requires a
high surface-to-volume ratio. The expansion of an organ’s
surface area is achieved by repeated branching of the
epithelial cell layer into the surrounding mesenchymal
tissue. The resulting branching
patterns differ according to the specific physiological
requirements of the respective organ. The lung and
kidney branching programs have been particularly well
studied: the key morphogens that control branching
morphogenesis in lungs and kidneys have been characterized
and early branching events are now known to
be highly stereotyped with few branching errors. However,
the underlying regulating mechanism is still elusive.

2D live imaging of organ explant cultures allows for monitoring branching morphogenesis over time and thereby comparing the branching behaviour of different organs or of different culture conditions. While analysing such temporal imaging data in a qualitative way is straightforward, inferring quantitative morphometric data with a high temporal resolution means a lot of manual work, especially if only specific organ parts such as single branches are of interest. In order to facilitate and automatise the quantitative analysis, we established an analysis pipeline that extracts essential morphometric features from acquired imaging data and compiles these information in a spatio-temporal manner, while limiting manual work to single correction steps. The image processing step mainly involves the segmentation and skeletonization of the branched tissue which enables the measurement of branch lengths and widths. Following single branches over time requires the generation of branch lineages and the integration of the measured morphometric features. These branch lineages then allow for evaluating quantitative branching data by branch generation, branch type and branching event. 

Image Processing
------------
The images were segmented in Fiji (ImageJ v1.52t) by using a global histogram-derived thresholding method (Schindelin et al., 2012). Prior to segmentation, the contrast of the images was increased with the built in Fiji plugins Enhance Local Contrast (CLAHE) and Substract Background. Next, the images were binarized with the built in Fiji plugin Auto Threshold by using the Default method. Boundaries were then smoothened with the built in Fiji plugin Gaussian Blur. Skeletonization of the binary images was performed with the built in Fiji plugin Skeletonize3D. The Fiji plugin AnalyzeSkeleton was used to infer the coordinates and the length of all branches of a skeleton (Arganda-Carreras et al., 2010). The branch widths were measured with the Thickness plugin that is included in the Fiji plugin BoneJ (v1.4.2) (Doube et al., 2010).

<img width="400" src="images/image-processing.png">

A: Raw image; B: Binary image; C: Skeleton; D: Thickness map

Branching analysis: How it works
------------
Analyzing the branching behavior over time requires the generation of lineages of all branches within a time series of skeletons. To this end, a MATLAB script was created to assign labels to all branches of a time series and to map these to each other across all time points. 

The script uses three classes (tree, branch and node) that contain class-specific properties.

<img width="200" src="images/class-structure.png">

### Tree
A tree stores the information of a skeleton at one specific time point and includes a list of all related branch and node labels. The mapping of trees across several time points is performed by finding the closest node in the node list of the next tree and thereby relating the corresponding branch labels to each other. In case the matching process is not successful at a certain point it is possible to remove single branches and to assign nodes manually to each other (see [Performing a branching analysis](#-performing-a-branching-analysis)).

### Branch
A branch is defined by its start and end node and an assigned index label. Every branch has one parent branch and at least one daughter branch which correspond to the index labels of this branch at the previous and next time point, respectively. Branch length and width are inferred from the image analysis results (see [Image Processing](#image-processing)). The branch type distinguishes between branches which are not connected to any other branch at one of their nodes (terminal branches) and branches which are connected to other branches at both of their nodes (non-terminal branches). Each branch contains information on the time point at which it appears for the first time, on its orientation and the angles relative to its connected branches, on the sector of the tree it belongs to and on its order which describes after how many branching events it has emerged and thereby denotes its generation number. When processing the first time point, a trunk branch can be defined as first generation corresponding to the trachea or ureter in the case of the lung or kidney, respectively, and the order of all other branches is then accordingly assigned. Alternatively, the orders for all branches that are present at the first time point can be defined manually and are then automatically updated and assigned for all subsequent time points. 

Every time a new branch appears a branching event is assigned to the parent branch which can be either terminal or lateral depending on whether the branching event is located at a terminal or non-terminal branch, respectively. The newly emerged branch will be labeled as additional daughter branch of the parent branch. This parent-daughter relationship allows for following a certain branch over time.

### Node
The nodes are determined by the coordinates that have been inferred by image skeletonization (see [Image Processing](#image-processing)) and contain index labels and the list of branches they belong to. 

Installation
--------------------------------
To run a branching analysis, please download the latest release of [MATLAB](https://www.mathworks.com/downloads/) 
and the [source code](https://git.bsse.ethz.ch/iber/Publications/2021_conrad_runser_biased-elongation/-/archive/master/2021_conrad_runser_biased-elongation-master.zip?path=branching-analysis).

Please extract the content and proceed to the next section titled [Performing a branching analysis](#performing-a-branching-analysis) for further instructions.

Performing a branching analysis
--------------------------------
The script requires several input files to perform the lineage generation. All required input files can be generated by following the image pre-processing steps described in the section [Image Processing](#image-processing). The input files need to be organized in two folders:

1. 'Output' folder: skeletons as png files and the branch coordinates as well as branch lengths saved in the '*_BranchInfo.csv' files

<img width="200" src="images/skeleton.png">
<img width="400" src="images/branch-info.png">

2. 'BoneJ' folder: thickness heat maps as tif files

<img width="200" src="images/thickness-map.png">

In order to start the lineage generation, run the script 'branching_analysis.m' in MATLAB. A 'Plot' folder is created automatically to collect images that show the matching results between consecutive timepoints ('Lineage_TX-Y.png') and images that display the branch and node indexes of single timepoints ('numbersNodes_TX.png'). These images should be used to verify a correct matching process. The node matching process between consecutive timepoints will be tracked by creating lineage csv files in the 'Output' folder ('NodeLineage_TX-Y.csv').

### Initialise branch generation

When the first time point is being processed, you have to choose if you want to define the branch generations (branch orders) of the first time point automatically by defining a trunk branch or manually by providing a 'BranchGeneration.csv' file.

<img width="400" src="images/branch-generation.png">

If you decide for the automatic way, click 'CANCEL' and you will then be asked to enter the index of the trunk branch. 

<img width="400" src="images/trunk-branch.png">

The index number can be inferred from the 'numbersindex_T1.png' file in the 'Plot' folder.

<img width="400" src="images/first-timepoint.png">

In case you are analyzing images of mouse lung or kidney explant cultures, the trunk branch corresponds to the trachea or ureter respectively. The generation of all other branches of the first timepoint is then assigned accordingly.

If you decide for the manual way, you need to generate a 'BranchGeneration.csv' file in which you assign a generation number to every branch of the first time point.

<img width="400" src="images/branchgen-file.png">

After specifying the initial branch generations, the script will start to match the node index labels between consecutive timepoints and create an image of the matching result in the 'Plot' folder and a lineage csv file in the 'Output' folder which tracks the node matches between consecutive timepoints.

### Problems in node matching
In case the script fails in matching the branch nodes of one timepoint to the consecutive one, you need to determine the cause for it. The first possible cause is a branch that is not present any more at the next timepoint ('A branch disappeared') which is mainly caused by inaccuracies during image binarisation and skeletonisation (see [Image Processing](#image-processing)). The second possible cause is a simple 'Node mismatch' which mainly occurs for relatively short branches where several nodes lie very closely together.

<img width="400" src="images/problem.png">

Please use the 'Lineage-TX-Y.png' image in the 'Plot' folder to determine the cause of the mismatch. 

#### 1) A branch disappeared

If you notice that a branch disappeared from the next timepoint, please select the option 'A branch disappeared'.

<img width="400" src="images/branch-disapp2.png">
<img width="400" src="images/branch-disapp1.png">

The script will now start to remove this branch from all previous timepoints. Therefore, you will be asked if you already know the index number of the branch that needs to be removed from the current timepoint. If you do so, please click 'Yes' and then enter the branch index number. This procedure needs to be repeated for all previous timepoints in which the branch were present. The script tracks these branch removals in the 'Remove.csv' file in the 'Output' folder.

<img width="400" src="images/branch-disapp3.png">
<img width="400" src="images/branch-disapp4.png">

If you don't yet know the index number of the branch that needs to be removed, please click 'No' and an image of the skeleton including branch index numbers for the current timepoint is created in the 'Plot' folder ('numbersindex_TX.png'). This image should allow you to identify the index number of the branch that needs to be removed.

<img width="400" src="images/branch-disapp5.png">

This procedure needs to be repeated for all previous timepoints in which the branch were present. The script tracks these branch removals in the 'Remove.csv' file in the 'Output' folder.

<img width="400" src="images/remove.png">

#### 2) Node mismatch

If you notice that nodes were matched incorrectly, please select the option 'Node mismatch'.

<img width="400" src="images/node-mis1.png">
<img width="400" src="images/node-mis2.png">

The script will then create images with the skeletons and node index numbers for the two respective timepoints ('numbersNodes_TX.png').

<img width="400" src="images/node-mis3.png">
<img width="400" src="images/node-mis4.png">

The node mismatch needs to be corrected in the corresponding 'NodeLineage_TX-Y.csv' file in the 'Output' folder.

<img width="400" src="images/node-mis5.png">
<img width="400" src="images/node-mis6.png">

### Output files

When the script finished successfully, there will be several result files available for further analysis:

1. 'Plot' folder: lineage plots of consecutive timepoints ('Lineage_TX-Y.png') and plots with branch and node indices for each timepoint ('numberNodes_TX.png')

2. 'Output' folder: node matching list of consecutive timepoints ('NodeLineage_TX-Y.csv') and the list of removed branches ('Remove.csv')

3. '00_Summaries' folder: lineage summary file including all branch properties such as length, width, branching events, etc.

<img width="400" src="images/summary.png">
