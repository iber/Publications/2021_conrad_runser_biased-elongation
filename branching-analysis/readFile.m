% SCRIPT TO READ AN INPUT FILE %
%
% Mathilde Dumond
%
%
function [currTree] = readFile(filename,directory)
    num = csvread(filename,1,0);
    timet = strsplit(filename,'_');
    time = timet{end-1};
    time = str2double(time(end-1:end));
    if isnan(time)
        time = timet{end-2};
        time = str2double(time(end-1:end));
    end

    currTree = Tree(time);
    listOfNodes = [];
    if size(num,2)==13
        i=0;
    elseif size(num,2)==12
        i=1;
    else
        error('The BranchInfo.csv file should have either 12 or 13 columns. Check the BranchInfo file: If it was changed, the code needs to be updated.')
    end
    for l = 1:size(num,1)
        line = num(l,:);
        if i==0
            currBranch = Branch(line(1-i));
            currBranch.time = time;
        elseif i==1
            currBranch = Branch(l);
            currBranch.time = time;
        end
        currBranch.subTree = line(2-i);

        coords = line((4-i):(6-i));

        [currN, listOfNodes] = checkNewNode(coords, listOfNodes);

        currN.addBranch(currBranch)

        currBranch.addNode(currN)

        coords = line((7-i):(9-i));
        [currN, listOfNodes] = checkNewNode(coords, listOfNodes);
        currN.addBranch(currBranch)
        currBranch.addNode(currN)
        length = line(3-i);
        currBranch.setLength(length)
        Elength = line(10-i);
        currBranch.setElength(Elength)

        currTree.addBranch(currBranch)
    end
    currTree.setNodeList(listOfNodes)
    currTree.setBranchesTypes()
end

function [node, listOfNodes] = checkNewNode(coords, listOfNodes)

    found = 0;
    for n = 1:size(listOfNodes,2)
        if coords==listOfNodes(n).coords
            node = listOfNodes(n);
            found = 1;
        end
    end
    if found == 0
        i=max(size(listOfNodes))+1;
        node = Node(i, coords);
        listOfNodes = [listOfNodes, node];
    end
end