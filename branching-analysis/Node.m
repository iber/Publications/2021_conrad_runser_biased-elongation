% SCRIPT TO STORE NODE INFO %
%
% Mathilde Dumond
%
%
classdef Node < handle

    properties
        coords
        index
        branchList
        type
        parent
        daughter

    end
    
    methods       
        function obj=Node(i,coords)            
            if nargin > 0
                obj.coords = coords;
                obj.branchList = [];
                obj.index = i;
            end
        end
        
        function [] = addBranch(obj, br)
            obj.branchList = [obj.branchList, br];
        end
        
        function [nodeAft] = findNearestPoint(obj, listOfPoints)
            thresh = 80;
            tempL = [];
            xm = obj.coords(1)-thresh;
            xM = obj.coords(1)+thresh;
            ym = obj.coords(2)-thresh;
            yM = obj.coords(2)+thresh;
            for i = 1:size(listOfPoints,2)
                if (listOfPoints(i).coords(1)>xm) && (listOfPoints(i).coords(1)<xM)
                    tempL = [tempL, listOfPoints(i)];
                end
            end
            tempL2 = [];
            for i = 1:size(tempL,2)
                if (tempL(i).coords(2)>ym) && (tempL(i).coords(2)<yM)
                    tempL2 = [tempL2, tempL(i)];
                end
            end
            if size(tempL2,2)==1
                nodeAft = tempL2(1);
            else
                if size(tempL2,2)==0
                    tempL2 = listOfPoints;
                end
                d = thresh^2;
                for i = 1:size(tempL2,2)
                    if obj.dist(tempL2(i)) < d
                        d = obj.dist(tempL2(i));
                        temp = tempL2(i);
                    end
                end
                nodeAft = temp;
            end
        end
        
        function d = dist(obj, node)
            d = sqrt((obj.coords(1)-node.coords(1))^2+(obj.coords(2)-node.coords(2))^2);
        end
        
        function c = imageCoords(obj)
            c = [obj.coords(2)+1, obj.coords(1)+1];
        end
        
        function n = higherNode(obj, n2)
            if obj.coords(2) > n2.coords(2)
                n=obj;
            else
                n=n2;
            end
        end
    end
end