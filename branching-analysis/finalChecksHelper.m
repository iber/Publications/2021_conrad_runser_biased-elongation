% SCRIPT TO DO FINAL CHECKS %
%
% Mathilde Dumond
%
%
function [res] = finalChecksHelper(allTrees)
res = 0;
for t=allTrees(2:end)
    for b=t.branchList
        b.time = t.time;
        b.setUndirLineage();
        if (numel(b.parent)==0) && (numel(b.undirParent)==0)
            fprintf('Time %s, branch %s has no parent\n',num2str(t.time), num2str(b.index))
            res = res+1;
        end
    end
end
end