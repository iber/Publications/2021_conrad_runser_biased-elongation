% SCRIPT TO DO FINAL CHECKS %
%
% Mathilde Dumond
%
%
function [] = finalChecks(allTrees)
res = 1;
while res ~=0
    res = finalChecksHelper(allTrees);
end
end