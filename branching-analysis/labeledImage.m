% SCRIPT TO MAP PIXELS TO BRANCH NUMBERS %
%
% Mathilde Dumond
%
%
function [im2] = labeledImage(tree, im)
    im2 = im;
    labJ = size(tree.branchList,2)+1;
    im2 = initialize(im2);
    im2 = labelJoints(tree, im2, labJ);
    lab = labJ+1;
    listN = tree.nodeList;
    loop = 0;
    listFinal = {};
    while size(listN, 2)>0 
        n = listN(1);
        [go, newc, im2] = findNewBranch(n, im2, lab,listFinal, labJ);
        if go==1
            loop = 0;
            [im2, listFinal] = analyzeBr(im2, lab, newc, labJ, listN, n, listFinal, tree);
            lab = lab+1;
        elseif go==2
            loop = 0;
            listN = listN(2:end);
            go=0;
        else
            listN = listN([2:end, 1]);
            if loop == 0
                loop = n;
            elseif loop == n
                disp(numel(listN))
                disp('Remaining nodes:')
                for n=listN
                    fprintf('%s, coords: %s, %s\n',...
                        num2str(n.index),...
                        num2str(n.coords(1)), num2str(n.coords(2)))
                end
                tree.plotTree('active', 'numbersNodes')
                figure;
                image(im2,'CDataMapping','scaled')
                colorbar
                error('We looped over all nodes.')
            end
        end
    end
    [im2] = renumberBranches(im2, listFinal, labJ);
end

function [im2] = initialize(im)
    im2 = zeros(size(im));
    for i=1:size(im2,1)
        for j=1:size(im2,2)
            if im(i,j) ~=0
                im2(i,j)=-1;
            end
        end
    end
end

function [im2, listFinal] = analyzeBr(im2, lab, c, labJ, listN, n, listFinal, tree)
    temp = im2(max(1,c(1)-1):c(1)+1, max(1,c(2)-1):c(2)+1);
    compteur = 0;
    finished = 0;
    while finished == 0
        compteur = compteur +1;
        if (ismember(labJ,temp) && compteur>2) || (size(find(temp == labJ),1)==2) 
            [listN, listFinal, finished] = branchTerminator(listN, n, temp, labJ,...
                listFinal, tree, c, lab, im2);
        else
            try 
                [nxt] = getNextPixel(temp);
            catch
                tempLarge = im2(max(1,c(1)-3):c(1)+3, max(1,c(2)-3):c(2)+3);
                tempLarge5 = im2(max(1,c(1)-5):c(1)+5, max(1,c(2)-5):c(2)+5);
                if compteur == 1
                    disp(c)
                    disp(tempLarge)
                    disp(tempLarge5)
                    error('Could not find nxt (compteur = 1)')
                elseif ismember(labJ, tempLarge)
                    [listN, listFinal, finished] = branchTerminator(listN, n, tempLarge, labJ,...
                        listFinal, tree, c, lab, im2);
                elseif ismember(labJ, tempLarge5)
                    [listN, listFinal, finished] = branchTerminator(listN, n, tempLarge5, labJ,...
                        listFinal, tree, c, lab, im2);
                else
                    disp(c)
                    disp(im2(c(1)-5:c(1)+5, c(2)-5:c(2)+5))
                    image(im2,'CDataMapping','scaled')
                    colorbar
                    error('Could not find nxt')
                end
            end
            if finished == 0
                if numel(temp)==9
                    i = mod(nxt-1,3)-1;
                    j = floor((nxt-1)/3)-1;
                else
                    i = mod(nxt-1,2);
                    j = floor((nxt-1)/2)-1;
                end
                if im2(c(1)+i, c(2)+j)==-1
                    im2(c(1)+i, c(2)+j) = lab;
                else
                    disp('output of the problem')
                    disp(temp)
                    disp(nxt)
                    disp([i,j])
                    disp(im2(c(1)+i, c(2)+j))
                    disp(c)
                    disp(compteur)
                    image(im2,'CDataMapping','scaled')
                    line([c(2)-10 c(2)-10 c(2)+10 c(2)+10 c(2)-10],...
                        [c(1)-10 c(1)+10 c(1)+10 c(1)-10 c(1)-10],'color','r')
                    colorbar
                    error(strcat('There is a problem shown in the red box.',...
                        ' It could be a small branch which complicates the treatment.',...
                        ' If it has no impact on your data, please remove it manually.',...
                        ' Run Analyze Skeleton 2D/3D, and replace the BranchInfo.csv file AND',...
                        ' the -labeled-skeletons.png file in the Output folder. If not, talk to Mathilde.'))
                end
            end
            c = [c(1)+i, c(2)+j];
            temp = im2(max(1,c(1)-1):c(1)+1, max(1,c(2)-1):c(2)+1);
        end
    end
end

function [go, newc, im2] = findNewBranch(n, im2, lab, listFinal, labJ)
    c = n.imageCoords();
    temp = im2(max(1,c(1)-1):c(1)+1, max(1,c(2)-1):c(2)+1);
    checkIsNode(c, n, im2)
    if size(find(temp == -1),1)==0
        go = 2;
        newc = 0;
        return
    end
    [tofinish, im2] = checkNodeAlready(n, im2, listFinal, labJ);
    if tofinish == 1
        go = 2;
        newc = 0;
        return
    end
    if ((countF(n,listFinal) == numel(n.branchList)-1) ||...
            ((countF(n,listFinal) == numel(n.branchList)-2) && size(find(temp==labJ)==2,1)==2)) &&...
            (size(find(temp==-1),1)==2)
        f=find(temp==-1);
        for r=1:size(f,1)
            if mod(f(r),2)==0
                nxt = f(r);
            end
        end
        if numel(temp)==9
            i = mod(nxt-1,3)-1;
            j = floor((nxt-1)/3)-1;
        else
            i = mod(nxt-1,2);
            j = floor((nxt-1)/2)-1;
        end
        im2(c(1)+i, c(2)+j) = lab;
    end
    newf = findIsolated(temp);
    if not(newf==0)
        if numel(temp)==9
            i = mod(newf-1,3)-1;
            j = floor((newf-1)/3)-1;
        else
            i=mod(newf-1,2);
            j=floor((newf-1)/2)-1;
        end
        newc = [c(1)+i, c(2)+j];
        isol = checkReallyIsolated(newc, im2);
        if isol==0
            newc = 0;
            go = 0;
            return
        end
        temp2 = im2(max(1,newc(1)-1):newc(1)+1, max(1,newc(2)-1):newc(2)+1);
        if (size(find(temp2==-1),1)==1) && (size(find(temp2==labJ),1)==1)
            for ne=[2,4,6,8]
                if (temp2(ne) ~= 0) && (temp2(ne) ~= labJ)
                    im2(newc(1),newc(2)) = temp2(ne);
                end
            end
            newc = 0;
            go = 0;
            return
        end
        im2(newc(1),newc(2)) = lab;
        go = 1;
        return
    end
    go = 0;
    newc = 0;
end

function [tofinish, im2] = checkNodeAlready(n, im2, listFinal, labJ)
    c = n.imageCoords();
    temp = im2(max(1,c(1)-1):c(1)+1, max(1,c(2)-1):c(2)+1);
    neighbors = [4,2;1,3;2,6;1,7;0,0;3,9;4,8;7,9;6,8];
    if (countF(n,listFinal) == numel(n.branchList)) ||...
            ((countF(n,listFinal) == numel(n.branchList)-1) && size(find(temp==labJ)==2,1)==2) 
        f=find(temp==-1);
        for pi=1:size(f,1)
            p=f(pi);
            neig = neighbors(p,:);
            for ne=neig
                if (temp(ne) ~= 0) && (temp(ne) ~= temp(5))
                    i = mod(p-1,3)-1;
                    j = floor((p-1)/3)-1;
                    im2(c(1)+i,c(2)+j) = temp(ne);
                    tofinish = 1;
                    return
                end
            end
        end
    end
    tofinish = 0;
end

function isol = checkReallyIsolated(newc, im2)
    temp = im2(max(1,newc(1)-1):newc(1)+1, max(1,newc(2)-1):newc(2)+1);
    if size(find(temp == -1),1)<=2
        isol = 1;
    else
        isol = 0;
    end
end

function newf = findIsolated(temp)
    f=find(temp==-1);
    corresp = [4,2;1,3;2,6;1,7;0,0;3,9;4,8;7,9;6,8];
    correspLarge = [4,2,0,0;1,3,4,6;2,6,0,0;1,7,2,8;0,0,0,0;3,9,2,8;4,8,0,0;7,9,4,6;6,8,0,0];
    newf = 0;
    for pi=1:size(f,1)
        p=f(pi);
        if sum(ismember(correspLarge(p,:),f))==0
            newf = p;
            return
        end
    end
end

function [] = checkIsNode(c, n, im2)
    if (im2(c(1),c(2))==0) || (im2(c(1),c(2))==-1)
        temp = im2(c(1)-1:c(1)+1, c(2)-1:c(2)+1);
        disp('temp:')
        disp(temp)
        disp('coords of the node that we could not find:')
        disp(c)
        disp(n.coords)
        image(im2,'CDataMapping','scaled')
        colorbar
        error('The node is not where I think it is')
    end
end

function im2 = labelJoints(tree, im, labJ)
    im2 = im;
    for n = tree.nodeList
        c = n.imageCoords();
        if im2(c(1),c(2))==-1
            im2(c(1),c(2))=labJ;
        end
    end
    disp('Labelling nodes')
end

function newn = findNode(iN,jN,listNodes, im2)
    newn = 0;
    for n = listNodes
        nt = n.imageCoords();
        if isequal(nt, [iN,jN])
            newn = n;
        end
    end
    if newn == 0
        disp('coords of the node that we could not find:')
        disp([iN,jN])
        image(im2,'CDataMapping','scaled')
        colorbar
        error('we could not find the node')
    end
end

function [nxt] = getNextPixel(temp)
    if size(find(temp == -1),1)==1
        nxt = find(temp==-1);
    elseif size(find(temp == -1),1)==2
        return
        nxt = 0;
        f = find(temp==-1);
        for r=1:size(f,1)
            if mod(f(r),2)==0
                nxt = f(r);
            end
        end
        if nxt==0
            disp(temp)
            disp(c)
            image(im2,'CDataMapping','scaled')
            colorbar
            error(...
                'error when we have 2 times -1 at the end of a branch')
        end
    end
end

function [listN, listFinal, finished] = branchTerminator(...
    listN, n, temp, labJ, listFinal, tree, c, lab, im2)
    listN = listN(listN~=n);
    if size(temp,1)==size(temp,2)
        iN = c(1)+mod(find(temp==labJ)-1,size(temp,1))-floor(size(temp,1)/2);
        jN = c(2)+floor((find(temp==labJ)-1)/size(temp,1))-floor(size(temp,1)/2);
    elseif (size(temp,2)>size(temp,1)) && (c(1)<2)
        diff = size(temp,2)-size(temp,1);
        iN = c(1)+mod(find(temp==labJ)-1,size(temp,1))-floor(size(temp,1)/2)+diff;
        jN = c(2)+floor((find(temp==labJ)-1)/size(temp,1))-floor(size(temp,1)/2);
    else
        error('The only side allowed to have branches is the up side. Correct so that no branch comes out of the window at any other place.')
    end
        nodeL = [];
        listNodes = tree.nodeList;
        for i = 1:size(iN)
            nodeL = [nodeL, findNode(iN(i),jN(i),listNodes, im2)];
            if not(findBranch(n, nodeL(i)) ==0)
                s1.label = lab; s1.node1 = n; s1.node2 = nodeL(i);
                listFinal = [listFinal, s1];
                finished = 1;
                return
            end
        end
        tree.plotTree('active','numbers')
        figure;
        parulamod = [1,1,1;parula(100)];
        image(im2,'CDataMapping','scaled')
        colormap(parulamod)
        colorbar
        error('could not find the branch')
    finished = 0;
    return
end

function br = findBranch(node1, node2)
    br = 0;
    for b1 = node1.branchList
        if ismember(b1, node2.branchList)
            br = b1;
        end
    end
    if br==0
        disp(node1.coords)
        disp(node2.coords)
        disp('branches node 1')
        for b=node1.branchList
            disp(b.index)
        end
        disp('branches node 2')
        for b=node2.branchList
            disp(b.index)
        end
        disp('Warning: I could not find a branch with those 2 nodes')
    end
end

function [im3] = renumberBranches(im2, listFinal, labJ)
    im3 = im2;
    corresp = zeros(labJ-1, 2);
    for b = listFinal
        br = findBranch(b{1}.node1, b{1}.node2);
        corresp(b{1}.label-labJ,1) = b{1}.label-labJ;
        corresp(b{1}.label-labJ,2) = br.index;
    end
    for i=1:size(im2,1)
        for j=1:size(im2,2)
            if (im2(i,j) > labJ) && (im2(i,j) ~= -1)
                try
                    im3(i,j) = corresp(im2(i,j)-labJ,2);
                catch
                    disp(im2(i,j))
                    disp([i,j])
                    disp(im2(i,j)-labJ)
                    disp(size(corresp))
                    disp(corresp)
                    error('aa')
                end
            end
        end
    end
end

function nb = countF(n,listFinal)
    nb = 0;
    for rep = listFinal
        if rep{1}.node1 == n
            nb = nb +1;
        end
        if rep{1}.node2 == n
            nb = nb +1;
        end
    end
end