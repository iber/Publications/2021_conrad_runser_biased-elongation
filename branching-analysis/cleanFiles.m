% SCRIPT TO CLEAN FILES %
%
% Mathilde Dumond
%
%
function files = cleanFiles(files)

rem = [];
for f=1:size(files,1)
    if strcmp(files(f).name(1:2), '._')==1
        rem = [rem, f];
    end
end
files(rem) = [];
end