% SCRIPT TO ANALYZE BRANCHING DATASETS %
%
% Mathilde Dumond
%
%
directory = uigetdir('','Select directory for experiment analysis');
direct_branchInfo = strcat(directory,'/Output/');
mkdir(directory,'/Plot');
str_ori = strcat(direct_branchInfo,'*BranchInfo.csv');
files = dir(str_ori); 
files = cleanFiles(files);
allTrees = Tree.empty(0,size(files,1));
rootnames = {};
removeFile = strcat(direct_branchInfo,'Remove.csv');
boneJ = 1;
for f=1:size(files,1)
    rootname = strsplit(files(f).name,'_');
    rootname = strjoin(rootname(1:end-1),'_');    
    rootnames{f} = rootname;
    fprintf('Reading the file: %s \n',files(f).name)
    allTrees(f) = readFile(strcat(direct_branchInfo,files(f).name), directory);
    temp = allTrees(f);

    imagename = strcat(rootname,'.png');
    brname = strcat(direct_branchInfo,imagename(1:end-4), '_branchesLab.tif');
        if not(exist(brname, 'file'))
            fprintf('Reading the image: %s\n',imagename)
            im = imread(strcat(direct_branchInfo,imagename));            
            [imSave] = labeledImage(allTrees(f), im);
            brname = strcat(direct_branchInfo,imagename(1:(end-4)), '_branchesLab.tif');
            imSave = single(imSave);
            s = size(im);
            t = Tiff(brname, 'w');
            tagstruct.ImageLength     = s(1);
            tagstruct.ImageWidth      = s(2);
            tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
            tagstruct.BitsPerSample   = 32;
            tagstruct.SamplesPerPixel = 1;
            tagstruct.RowsPerStrip    = 1000;
            tagstruct.ExtraSamples    = Tiff.ExtraSamples.AssociatedAlpha;
            tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
            tagstruct.Software        = 'MATLAB';
            tagstruct.SampleFormat=Tiff.SampleFormat.IEEEFP;
            t.setTag(tagstruct);
            t.write(imSave)
            t.close()
        end
        
        allTrees(f).readBranchIm(brname)
        str = strcat(directory, '/BoneJ/',rootname,'*');
        boneJname = dir(str);
        disp('#### BONEJ ####')        
        try
            disp(boneJname.name)
            allTrees(f).readBoneJ(strcat(directory, '/BoneJ/',...
                boneJname.name))
        catch
            error('The BoneJ files are missing')
            boneJ = 0;
        end

    allTrees(f).checkNodesTwoBr()
    allTrees(f).checkSmallBr()
    allTrees(f) = checkRemoveSingle(removeFile, allTrees(f));
    allTrees(f).checkNodesTwoBr()

    cleanSubTrees(allTrees(f))
    if f>1
        setLineage(allTrees(f), allTrees(f-1), directory, allTrees, allTrees(f).time)
    else
        allTrees(f).setBrOrder(directory)
    end
    
    allTrees(f).setBrWidth()
    allTrees(f).checkDaught()
    if ismember(allTrees(f).time, []) ==1
        plotTree(allTrees(f),'active','numbersindex')
        plotTree(allTrees(f),'active','numbersorder')
    end
    allTrees(f).defineBranchSectors()
    if ismember(allTrees(f).time, []) ==1
        plotTree(allTrees(f),'active','numbersindex')
        plotTree(allTrees(f),'active','numbersNodes')
        plotTree(allTrees(f),'active','numberssector')
        plotTree(allTrees(f),'active','numbersorder')
    end 
    allTrees(f).defineBranchOrientation()
    if ismember(allTrees(f).time, []) ==1
        plotTree(allTrees(f),'active','numbersindex')
        plotTree(allTrees(f),'active','numberssector')
        plotTree(allTrees(f),'active','numbersorientation')
    end
    allTrees(f).plotTree(directory, 'numbersNodes')
end

finalChecks(allTrees)

exportData(allTrees, directory)