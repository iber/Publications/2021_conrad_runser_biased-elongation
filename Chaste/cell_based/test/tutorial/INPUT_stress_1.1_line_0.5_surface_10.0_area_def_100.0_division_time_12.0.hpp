

#ifndef TESTRUNNINGVERTEXBASEDSIMULATIONSTUTORIAL_HPP_
#define TESTRUNNINGVERTEXBASEDSIMULATIONSTUTORIAL_HPP_

#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"

  /* The remaining header files define classes that will be used in the cell-based
   * simulation. We have encountered some of these header files in previous cell-based
   * Chaste tutorials. */
#include "CellsGenerator.hpp"
#include "OffLatticeSimulation.hpp"
#include "TransitCellProliferativeType.hpp"
#include "SmartPointers.hpp"
   /* The next header file defines the cell cycle model. */
#include "UniformG1GenerationalCellCycleModel.hpp"
/* The next two header files define a helper class for generating suitable meshes: one planar and one periodic. */
#include "HoneycombVertexMeshGenerator.hpp"
#include "CylindricalHoneycombVertexMeshGenerator.hpp"
#include "VertexBasedCellPopulation.hpp"
/* The next header file defines a force law for describing the mechanical interactions
 * between neighbouring cells in the cell population, subject to each vertex.
 */
#include "NagaiHondaForce.hpp"
 /* This force law assumes that cells possess a "target area" property which determines the size of each
  * cell in the simulation. In order to assign target areas to cells and update them in each time step, we need
  * the next header file.
  */
#include "SimpleTargetAreaModifier.hpp"
  /* The next header file defines a boundary condition for the cells.*/
#include "PlaneBoundaryCondition.hpp"
/* The next header file defines a cell killer, which specifies how cells are removed from the simulation.*/
#include "PlaneBasedCellKiller.hpp"

/* Finally, we include a header that enforces running this test only on one process. */
#include "FakePetscSetup.hpp"


#include "NoCellCycleModel.hpp"
#include "CellLabel.hpp"
#include "FixedG1GenerationalCellCycleModel.hpp"
#include "DifferentiatedCellProliferativeType.hpp"

#include "ShearStressForceBothSides.hpp"
#include "MyNagaiHondaForce.hpp"
#include "CellAppliedForceWriter.hpp"
#include "NodeVelocityWriter.hpp"
#include "BoundaryNodeWriter.hpp"
#include "SideCellProperties.hpp"
#include "CellVolumesWriter.hpp"
#include "CellLocationIndexWriter.hpp"
#include "ForwardEulerNumericalMethod.hpp"
#include "AbstractNumericalMethod.hpp"

#include "NewChemotacticForce.hpp"
#include <string>

//My own cell cycle model, the g1 duration is randomly set, allows a wider cell area distribution
class MyCellCycleModel : public AbstractSimpleGenerationalCellCycleModel
{
private:

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)

	{
		archive & boost::serialization::base_object<AbstractSimpleGenerationalCellCycleModel>(*this);
		RandomNumberGenerator* p_gen = RandomNumberGenerator::Instance();
                p_gen->Reseed(time(NULL));
		archive & *p_gen;
		archive & p_gen;
	}

	void SetG1Duration()
	{
		assert(mpCell != NULL);

		double averageG1Duration = 12.0;
		double stdDev = averageG1Duration * 0.4;

		double uniform_random_number = RandomNumberGenerator::Instance()->NormalRandomDeviate(averageG1Duration, stdDev);
		uniform_random_number = std::max(0.2 * averageG1Duration, uniform_random_number);

		if (mpCell->GetCellProliferativeType()->IsType<StemCellProliferativeType>())
		{
			mG1Duration = uniform_random_number;
		}
		else if (mpCell->GetCellProliferativeType()->IsType<TransitCellProliferativeType>())
		{
			mG1Duration = uniform_random_number;
		}
		else if (mpCell->GetCellProliferativeType()->IsType<DifferentiatedCellProliferativeType>())
		{
			mG1Duration = DBL_MAX;
		}
		else
		{
			NEVER_REACHED;
		}
	}

public:

	MyCellCycleModel()
	{}

	AbstractCellCycleModel* CreateCellCycleModel()
	{
		MyCellCycleModel* p_model = new MyCellCycleModel();

		p_model->SetBirthTime(mBirthTime);
		p_model->SetMinimumGapDuration(mMinimumGapDuration);
		p_model->SetStemCellG1Duration(mStemCellG1Duration);
		p_model->SetTransitCellG1Duration(mTransitCellG1Duration);
		p_model->SetSDuration(0.0);
		p_model->SetG2Duration(0.0);
		p_model->SetMDuration(0.0);
		p_model->SetGeneration(mGeneration);
		p_model->SetMaxTransitGenerations(mMaxTransitGenerations);

		return p_model;
	}
};
#include "SerializationExportWrapper.hpp"
CHASTE_CLASS_EXPORT(MyCellCycleModel)
#include "SerializationExportWrapperForCpp.hpp"
CHASTE_CLASS_EXPORT(MyCellCycleModel)




class TestRunningVertexBasedSimulationsTutorial : public AbstractCellBasedTestSuite
{
public:

	void TestMonolayer()
	{
		//Generate the honeycomb vertex mesh
		unsigned mesh_width = 10;
		unsigned mesh_heigth = 10;
		HoneycombVertexMeshGenerator generator(mesh_width, mesh_heigth);    // Parameters are: cells across, cells up
		MutableVertexMesh<2, 2>* p_mesh = generator.GetMesh();

		//Generate a vector of cell, with as many cells as VertexElements in the mesh
		std::vector<CellPtr> cells;
		MAKE_PTR(TransitCellProliferativeType, p_transit_type);
		MAKE_PTR(DifferentiatedCellProliferativeType, p_differentiated_type);

		//Generates the cell cycle model
		CellsGenerator<MyCellCycleModel, 2> cells_generator;
		cells_generator.GenerateBasicRandom(cells, p_mesh->GetNumElements(), p_transit_type);

		//Generate the pointers to tag the boundary cells
		MAKE_PTR(BottomCellProperty, p_bottom_cell);
		MAKE_PTR(TopCellProperty, p_top_cell);
		MAKE_PTR(LeftCellProperty, p_left_cell);
		MAKE_PTR(RightCellProperty, p_right_cell);

		//Vector where the excluded nodes will be stored (Node where external forces are applied)
		std::vector<CellPtr> exludedCellsBottom;
		std::vector<CellPtr> exludedCellsTop;


		for (unsigned i = 0; i < cells.size(); i++) {

			//If the cell is at the bottom of the tissue, we prevent it from dividing, and marked it with a specific label
			if (i < mesh_width) {
				//My proliferation model
				FixedG1GenerationalCellCycleModel* p_model_fixed = new FixedG1GenerationalCellCycleModel();
				cells[i]->SetCellCycleModel(p_model_fixed);
				cells[i]->SetCellProliferativeType(p_differentiated_type);
				cells[i]->AddCellProperty(p_bottom_cell);
				exludedCellsBottom.push_back(cells[i]);
			}
			//If the cell belongs to the top layer of the tissue
			else if (i >= cells.size() - mesh_width) {
				FixedG1GenerationalCellCycleModel* p_model_fixed = new FixedG1GenerationalCellCycleModel();
				cells[i]->SetCellCycleModel(p_model_fixed);
				cells[i]->SetCellProliferativeType(p_differentiated_type);
				cells[i]->AddCellProperty(p_top_cell);
				exludedCellsTop.push_back(cells[i]);
			}
			//If the cell is located on the left side
			else if (i % mesh_width == 0) {
				cells[i]->AddCellProperty(p_left_cell);
			}
			//If the cell is located on the right side
			else{
				cells[i]->AddCellProperty(p_right_cell);
			}
		}

		VertexBasedCellPopulation<2> cell_population(*p_mesh, cells);

		//Change the damping constant of the model
		//cell_population.SetDampingConstantMutant(0.1);
		//cell_population.SetDampingConstantNormal(0.1);

		//Add the output writer to monitor the forces applied on the cells
		cell_population.AddPopulationWriter<NodeVelocityWriter>();
		cell_population.AddPopulationWriter<BoundaryNodeWriter>();
		cell_population.AddCellWriter<CellVolumesWriter>();
		cell_population.AddCellWriter<CellLocationIndexWriter>();

		//Now we find the nodes belonging to the differentiated cells
		std::vector<Node<2>*> excludedNodesBottom = cell_population.getExludedNodes(mesh_width, exludedCellsBottom);
		std::vector<Node<2>*> excludedNodesTop = cell_population.getExludedNodes(mesh_width, exludedCellsTop);


		// Creates the offLatticeSimulator object, treats the forces and velocities throughout the simulation
		OffLatticeSimulation<2> simulator(cell_population);
		//Set the output drectory and the duration of the simulation (given in hours)

		simulator.SetOutputDirectory("OUTPUT_stress_1.1_line_0.5_surface_10.0_area_def_100.0_division_time_12.0_sample_1");
		simulator.SetEndTime(48.0);

		//Creates a numericalMethod Object (the one used to integrate the forces and calculate the node positions over time)
		ForwardEulerNumericalMethod<2,2>* p_numerical_method =  new ForwardEulerNumericalMethod<2,2>();
		p_numerical_method->SetUseAdaptiveTimestep(true);
		boost::shared_ptr<AbstractNumericalMethod<2, 2>> numerical_ptr(p_numerical_method);
		simulator.SetNumericalMethod(numerical_ptr);



		//AAdd the newly created shear stress force
		MAKE_PTR(ShearStressForceBothSides, p_shear_stress_force);
		p_shear_stress_force -> SetStrength(1.1);
		p_shear_stress_force -> SetExcludedNodes(excludedNodesBottom, excludedNodesTop);
        	simulator.AddForce(p_shear_stress_force);

		//Frequency of sampling (every 50 time steps)
		simulator.SetSamplingTimestepMultiple(50); //Normaly set to 50

		//Set the line tension of the vertex model
		MAKE_PTR(NagaiHondaForce<2>, p_force);
   		p_force->SetNagaiHondaDeformationEnergyParameter(100.0);
		p_force->SetNagaiHondaCellCellAdhesionEnergyParameter(0.5);
		p_force->SetNagaiHondaCellBoundaryAdhesionEnergyParameter(0.5);
		p_force->SetNagaiHondaMembraneSurfaceEnergyParameter(10.0);
		simulator.AddForce(p_force);


		MAKE_PTR(SimpleTargetAreaModifier<2>, p_growth_modifier);
		simulator.AddSimulationModifier(p_growth_modifier);

		//simulator.SetDt(0.002);


		try{
			simulator.Solve();
		}catch(std::exception& e){
			std::cout << e.what();
		}


		/* The next two lines are for test purposes only and are not part of this tutorial. If different simulation input parameters are being explored
		 * the lines should be removed.*/
	}


};

#endif /* TESTRUNNINGVERTEXBASEDSIMULATIONSTUTORIAL_HPP_ */
#pragma once
