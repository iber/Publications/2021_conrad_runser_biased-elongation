
#include "MyBoundaryNodeWriter.hpp"
#include "AbstractCellPopulation.hpp"
#include "MeshBasedCellPopulation.hpp"
#include "CaBasedCellPopulation.hpp"
#include "NodeBasedCellPopulation.hpp"
#include "PottsBasedCellPopulation.hpp"
#include "VertexBasedCellPopulation.hpp"

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
MyBoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::MyBoundaryNodeWriter()
	: AbstractCellPopulationWriter<ELEMENT_DIM, SPACE_DIM>("results.vizboundarynodes")
{
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void MyBoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::VisitAnyPopulation(AbstractCellPopulation<SPACE_DIM, SPACE_DIM>* pCellPopulation)
{

	// Iterate over vertex elements ///\todo #2512 - replace with loop over cells
	for (typename VertexMesh<DIM, DIM>::VertexElementIterator elem_iter = mpMutableVertexMesh->GetElementIteratorBegin();
		elem_iter != mpMutableVertexMesh->GetElementIteratorEnd();
		++elem_iter)
	{
		// Get index of this element in the vertex mesh
		unsigned elem_index = elem_iter->GetIndex();
		VertexElement<DIM, DIM>* p_element = mpMutableVertexMesh->GetElement(elem_index);


		// Get the cell corresponding to this element
		CellPtr p_cell = this->GetCellUsingLocationIndex(elem_index);
		assert(p_cell);

		//We loop locally over the node owned by the vertex object
		unsigned num_nodes = p_element->GetNumNodes();
		for (unsigned local_index = 0; local_index < num_nodes; local_index++) {
			Node<DIM>* mNode = p_element->GetNode(local_index);
			
			//If the node is a boudary node
			if (mNode->IsBoundaryNode()) {
				//Check if the cell is on the bottom or top layer
				if (p_cell->HasCellProperty<CellLabel>()) {
					*this->mpOutStream << "BT_" << mNode->rGetLocation() << " ";
				}
				//else, the node is on the right or left side
				else {
					*this->mpOutStream << "LR_" << mNode->rGetLocation() << " ";
				}
			}
		}
	}
}
	
	
	
	
	
	for (typename AbstractMesh<SPACE_DIM, SPACE_DIM>::NodeIterator node_iter = pCellPopulation->rGetMesh().GetNodeIteratorBegin();
		node_iter != pCellPopulation->rGetMesh().GetNodeIteratorEnd();
		++node_iter)
	{
		if (!node_iter->IsDeleted())
		{
			*this->mpOutStream << node_iter->IsBoundaryNode() << " ";
		}
	}
}


template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void MyBoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(VertexBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{
	VisitAnyPopulation(pCellPopulation);
}




// Explicit instantiation
template class BoundaryNodeWriter<1, 1>;
template class BoundaryNodeWriter<1, 2>;
template class BoundaryNodeWriter<2, 2>;
template class BoundaryNodeWriter<1, 3>;
template class BoundaryNodeWriter<2, 3>;
template class BoundaryNodeWriter<3, 3>;

#include "SerializationExportWrapperForCpp.hpp"
// Declare identifier for the serializer
EXPORT_TEMPLATE_CLASS_ALL_DIMS(BoundaryNodeWriter)

