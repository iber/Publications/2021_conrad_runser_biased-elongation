

//Constructor
template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
CellLewisLawWriter<ELEMENT_DIM, SPACE_DIM>::CellLewisLawWriter()
	: AbstractCellPopulationWriter<ELEMENT_DIM, SPACE_DIM>("lewis_law_results.dat")
{
}

//Visit method, used to get the data and write them in a file

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void CellLewisLawWriter<ELEMENT_DIM, SPACE_DIM>::Visit(VertexBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{
	// Loop over cells and find associated elements so in the same order as the cells in output files
	for (typename AbstractCellPopulation<SPACE_DIM, SPACE_DIM>::Iterator cell_iter = pCellPopulation->Begin();
		cell_iter != pCellPopulation->End();
		++cell_iter)
	{
		//Get the index of the elementn corresponding to the treated cell
		unsigned elem_index = pCellPopulation->GetLocationIndexUsingCell(*cell_iter);
		
		//Get the number of nodes of this element
		unsigned num_nodes_in_element = pCellPopulation->GetElement(elem_index)->GetNumNodes();

		//Get the volume of the cell
		double volume = pCellPopulation->GetVolumeOfCell(*cell_iter);


		//Write everything in the file
		*this->mpOutStream << "|" << num_nodes_in_element << " " << volume;

	}
}


#include "SerializationExportWrapperForCpp.hpp"
// Declare identifier for the serializer
EXPORT_TEMPLATE_CLASS_ALL_DIMS(CellLewisLawWriter)
