/*

Copyright (c) 2005-2018, University of Oxford.
All rights reserved.

University of Oxford means the Chancellor, Masters and Scholars of the
University of Oxford, having an administrative office at Wellington
Square, Oxford OX1 2JD, UK.

This file is part of Chaste.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the University of Oxford nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "BoundaryNodeWriter.hpp"
#include "AbstractCellPopulation.hpp"
#include "MeshBasedCellPopulation.hpp"
#include "CaBasedCellPopulation.hpp"
#include "NodeBasedCellPopulation.hpp"
#include "PottsBasedCellPopulation.hpp"
#include "VertexBasedCellPopulation.hpp"
#include "SideCellProperties.hpp"

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::BoundaryNodeWriter()
    : AbstractCellPopulationWriter<ELEMENT_DIM, SPACE_DIM>("results.vizboundarynodes")
{
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::VisitAnyPopulation(AbstractCellPopulation<SPACE_DIM, SPACE_DIM>* pCellPopulation)
{
    for (typename AbstractMesh<SPACE_DIM, SPACE_DIM>::NodeIterator node_iter = pCellPopulation->rGetMesh().GetNodeIteratorBegin();
         node_iter != pCellPopulation->rGetMesh().GetNodeIteratorEnd();
         ++node_iter)
    {
        if (!node_iter->IsDeleted())
        {
            *this->mpOutStream << node_iter->IsBoundaryNode() << " ";
        }
    }
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(MeshBasedCellPopulation<ELEMENT_DIM, SPACE_DIM>* pCellPopulation)
{
    for (typename AbstractMesh<ELEMENT_DIM, SPACE_DIM>::NodeIterator node_iter = pCellPopulation->rGetMesh().GetNodeIteratorBegin();
            node_iter != pCellPopulation->rGetMesh().GetNodeIteratorEnd();
            ++node_iter)
    {
        if (!node_iter->IsDeleted())
        {
            *this->mpOutStream << node_iter->IsBoundaryNode() << " ";
        }
    }
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(CaBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{
    VisitAnyPopulation(pCellPopulation);
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(NodeBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{
    VisitAnyPopulation(pCellPopulation);
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(PottsBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{
    VisitAnyPopulation(pCellPopulation);
}

template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
void BoundaryNodeWriter<ELEMENT_DIM, SPACE_DIM>::Visit(VertexBasedCellPopulation<SPACE_DIM>* pCellPopulation)
{

	//Retrieve the mesh for this cell population
	MutableVertexMesh<SPACE_DIM, SPACE_DIM>& mpMutableVertexMesh = pCellPopulation->rGetMesh();

	//Retrieve the iterator for this mesh
	


	// Iterate over vertex elements ///\todo #2512 - replace with loop over cells
	for (typename VertexMesh<SPACE_DIM, SPACE_DIM>::VertexElementIterator elem_iter = mpMutableVertexMesh.GetElementIteratorBegin(); 
		elem_iter != mpMutableVertexMesh.GetElementIteratorEnd();
		++elem_iter)
	{
		// Get index of this element in the vertex mesh
		unsigned elem_index = elem_iter->GetIndex();
		VertexElement<SPACE_DIM, SPACE_DIM>* p_element = mpMutableVertexMesh.GetElement(elem_index);

		// Get the cell corresponding to this element
		CellPtr p_cell = pCellPopulation->GetCellUsingLocationIndex(elem_index);
		assert(p_cell);

		//We loop locally over the node owned by the vertex object
		unsigned num_nodes = p_element->GetNumNodes();
		for (unsigned local_index = 0; local_index < num_nodes; local_index++) {
			Node<SPACE_DIM>* mNode = p_element->GetNode(local_index);
			//If the node is a boudary node
			if (mNode->IsBoundaryNode()) {
				//Check if the cell is on the bottom or top layer
				*this->mpOutStream << "|";

				if (p_cell->HasCellProperty<BottomCellProperty>()) {
					*this->mpOutStream << "B_";
				}
				//else, the node is on the right or left side
				else if (p_cell->HasCellProperty<TopCellProperty>()) {
					*this->mpOutStream << "T_";
				}
				//else, the node is on the right or left side
				else if (p_cell->HasCellProperty<LeftCellProperty>()) {
					*this->mpOutStream << "L_";
				}
				//else, the node is on the right or left side
				else if (p_cell->HasCellProperty<RightCellProperty>()) {
					*this->mpOutStream << "R_";
				}
				// Write this node's position to file
				const c_vector<double, SPACE_DIM>& position = mNode->rGetLocation();
				for (unsigned i = 0; i < SPACE_DIM; i++)
				{
					*this->mpOutStream << position[i] << " ";
				}
			}
		}
	}
}


// Explicit instantiation
template class BoundaryNodeWriter<1,1>;


#include "SerializationExportWrapperForCpp.hpp"
// Declare identifier for the serializer
EXPORT_TEMPLATE_CLASS_ALL_DIMS(BoundaryNodeWriter)

