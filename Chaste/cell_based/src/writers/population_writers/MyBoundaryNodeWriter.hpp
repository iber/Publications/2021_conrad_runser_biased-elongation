
#ifndef MYBOUNDARYNODEWRITER_HPP_
#define MYBOUNDARYNODEWRITER_HPP_

#include "AbstractCellPopulationWriter.hpp"
#include "ChasteSerialization.hpp"
#include <boost/serialization/base_object.hpp>

/**
 * A class written using the visitor pattern for writing the boundary nodes of a population to file.
 *
 * The output file is called results.vizboundarynodes by default.
 */
template<unsigned ELEMENT_DIM, unsigned SPACE_DIM>
class MyBoundaryNodeWriter : public AbstractCellPopulationWriter<ELEMENT_DIM, SPACE_DIM>
{
private:
	/** Needed for serialization. */
	friend class boost::serialization::access;
	/**
	 * Serialize the object and its member variables.
	 *
	 * @param archive the archive
	 * @param version the current version of this class
	 */
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractCellPopulationWriter<ELEMENT_DIM, SPACE_DIM> >(*this);
	}

public:

	/**
	 * Default constructor.
	 */
	MyBoundaryNodeWriter();

	/**
	 * Visit the population and write the data.
	 *
	 * @param pCellPopulation a pointer to the population to visit.
	 */
	void VisitAnyPopulation(AbstractCellPopulation<SPACE_DIM, SPACE_DIM>* pCellPopulation);


	/**
	 * Visit the population and write whether each node is a boundary node.
	 *
	 * Outputs a line of space-separated values of the form:
	 * [node 0 is boundary node] [node 1 is boundary node] ...
	 *
	 * where [node 0 is boundary node] is 1 if node 0 is a boundary node and 0 if it is not, and so on.
	 * Here the indexing of nodes is as given by the NodeIterator.
	 *
	 * This line is appended to the output written by AbstractCellBasedWriter, which is a single
	 * value [present simulation time], followed by a tab.
	 *
	 * @param pCellPopulation a pointer to the VertexBasedCellPopulation to visit.
	 */
	virtual void Visit(VertexBasedCellPopulation<SPACE_DIM>* pCellPopulation);
};

#include "SerializationExportWrapper.hpp"
// Declare identifier for the serializer
EXPORT_TEMPLATE_CLASS_ALL_DIMS(MyBoundaryNodeWriter)

#endif /*MYBOUNDARYNODEWRITER_HPP_*/


