#include "SideCellProperties.hpp"

//#########################################################################################

BottomCellProperty::BottomCellProperty(unsigned colour)
	: AbstractCellProperty(),
	mColour(colour)
{
}

BottomCellProperty::~BottomCellProperty()
{}

unsigned BottomCellProperty::GetColour() const
{
	return mColour;
}

//#########################################################################################

TopCellProperty::TopCellProperty(unsigned colour)
	: AbstractCellProperty(),
	mColour(colour)
{
}

TopCellProperty::~TopCellProperty()
{}

unsigned TopCellProperty::GetColour() const
{
	return mColour;
}

//#########################################################################################

LeftCellProperty::LeftCellProperty(unsigned colour)
	: AbstractCellProperty(),
	mColour(colour)
{
}

LeftCellProperty::~LeftCellProperty()
{}

unsigned LeftCellProperty::GetColour() const
{
	return mColour;
}

//#########################################################################################

RightCellProperty::RightCellProperty(unsigned colour)
	: AbstractCellProperty(),
	mColour(colour)
{
}

RightCellProperty::~RightCellProperty()
{}

unsigned RightCellProperty::GetColour() const
{
	return mColour;
}
