#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"
#include "AbstractCellProperty.hpp"
#include "CellLabel.hpp"

//#########################################################################################

class BottomCellProperty: public AbstractCellProperty
{
private:

	unsigned mColour;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractCellProperty>(*this);
		archive & mColour;
	}

public:

	BottomCellProperty(unsigned colour = 1);

	~BottomCellProperty();

	unsigned GetColour() const;
};

//#########################################################################################

class TopCellProperty : public AbstractCellProperty
{
private:

	unsigned mColour;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractCellProperty>(*this);
		archive & mColour;
	}

public:

	TopCellProperty(unsigned colour = 2);

	~TopCellProperty();

	unsigned GetColour() const;
};

//#########################################################################################

class LeftCellProperty : public AbstractCellProperty
{
private:

	unsigned mColour;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractCellProperty>(*this);
		archive & mColour;
	}

public:

	LeftCellProperty(unsigned colour = 3);

	~LeftCellProperty();

	unsigned GetColour() const;
};


//#########################################################################################

class RightCellProperty : public AbstractCellProperty
{
private:

	unsigned mColour;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractCellProperty>(*this);
		archive & mColour;
	}

public:

	RightCellProperty(unsigned colour = 4);

	~RightCellProperty();

	unsigned GetColour() const;
};
