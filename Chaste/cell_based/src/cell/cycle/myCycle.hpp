#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"

#include "SmartPointers.hpp"
#include "Exception.hpp"

#include "AbstractSimpleGenerationalCellCycleModel.hpp"

#include "CheckReadyToDivideAndPhaseIsUpdated.hpp"
#include "HoneycombMeshGenerator.hpp"
#include "WildTypeCellMutationState.hpp"
#include "GeneralisedLinearSpringForce.hpp"
#include "OffLatticeSimulation.hpp"
#include "StemCellProliferativeType.hpp"
#include "TransitCellProliferativeType.hpp"
#include "DifferentiatedCellProliferativeType.hpp"
//This test is always run sequentially (never in parallel)
#include "FakePetscSetup.hpp"

class myCycle : public AbstractSimpleGenerationalCellCycleModel
{
private:

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractSimpleGenerationalCellCycleModel>(*this);
		RandomNumberGenerator* p_gen = RandomNumberGenerator::Instance();
		archive & *p_gen;
		archive & p_gen;
	}

	void SetG1Duration();

public:

	myCycle();

	AbstractCellCycleModel* CreateCellCycleModel();
};

#include "SerializationExportWrapper.hpp"
CHASTE_CLASS_EXPORT(myCycle)

#include "SerializationExportWrapperForCpp.hpp"
CHASTE_CLASS_EXPORT(myCycle)