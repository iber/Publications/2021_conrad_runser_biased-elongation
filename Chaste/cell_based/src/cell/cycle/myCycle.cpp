#include "myCycle.hpp"

void myCycle::SetG1Duration()
{
	assert(mpCell != NULL);

	double uniform_random_number = RandomNumberGenerator::Instance()->ranf();

	if (mpCell->GetCellProliferativeType()->IsType<StemCellProliferativeType>())
	{
		mG1Duration = -log(uniform_random_number)*GetStemCellG1Duration();
	}
	else if (mpCell->GetCellProliferativeType()->IsType<TransitCellProliferativeType>())
	{
		mG1Duration = -log(uniform_random_number)*GetTransitCellG1Duration();
	}
	else if (mpCell->GetCellProliferativeType()->IsType<DifferentiatedCellProliferativeType>())
	{
		mG1Duration = DBL_MAX;
	}
	else
	{
		NEVER_REACHED;
	}
}


myCycle::myCycle():AbstractSimpleGenerationalCellCycleModel()
{}


AbstractCellCycleModel* myCycle::CreateCellCycleModel()
{
	myCycle* p_model = new  myCycle();

	p_model->SetBirthTime(mBirthTime);
	p_model->SetMinimumGapDuration(mMinimumGapDuration);
	p_model->SetStemCellG1Duration(mStemCellG1Duration);
	p_model->SetTransitCellG1Duration(mTransitCellG1Duration);
	p_model->SetSDuration(mSDuration);
	p_model->SetG2Duration(mG2Duration);
	p_model->SetMDuration(mMDuration);
	p_model->SetGeneration(mGeneration);
	p_model->SetMaxTransitGenerations(mMaxTransitGenerations);

	return p_model;
}


