#include "MyNagaiHondaForce.hpp"

template<unsigned DIM>
MyNagaiHondaForce<DIM>::MyNagaiHondaForce(std::vector<Node<DIM>*> rExcludedNodes)
	: NagaiHondaForce<DIM>(),
	mExcludedNodes(rExcludedNodes)
	{
		assert(mExcludedNodes.size() > 0);
	}






template<unsigned DIM>
void MyNagaiHondaForce<DIM>::AddForceContribution(AbstractCellPopulation<DIM>& rCellPopulation) {
	// Throw an exception message if not using a VertexBasedCellPopulation
	if (dynamic_cast<VertexBasedCellPopulation<DIM>*>(&rCellPopulation) == nullptr)
	{
		EXCEPTION("NagaiHondaForce is to be used with a VertexBasedCellPopulation only");
	}

	// Define some helper variables
	VertexBasedCellPopulation<DIM>* p_cell_population = static_cast<VertexBasedCellPopulation<DIM>*>(&rCellPopulation);
	unsigned num_nodes = p_cell_population->GetNumNodes();
	unsigned num_elements = p_cell_population->GetNumElements();

	// Begin by computing the area and perimeter of each element in the mesh, to avoid having to do this multiple times
	std::vector<double> element_areas(num_elements);
	std::vector<double> element_perimeters(num_elements);
	std::vector<double> target_areas(num_elements);
	for (typename VertexMesh<DIM, DIM>::VertexElementIterator elem_iter = p_cell_population->rGetMesh().GetElementIteratorBegin();
		elem_iter != p_cell_population->rGetMesh().GetElementIteratorEnd();
		++elem_iter)
	{
		unsigned elem_index = elem_iter->GetIndex();
		element_areas[elem_index] = p_cell_population->rGetMesh().GetVolumeOfElement(elem_index);
		element_perimeters[elem_index] = p_cell_population->rGetMesh().GetSurfaceAreaOfElement(elem_index);
		try
		{
			// If we haven't specified a growth modifier, there won't be any target areas in the CellData array and CellData
			// will throw an exception that it doesn't have "target area" entries.  We add this piece of code to give a more
			// understandable message. There is a slight chance that the exception is thrown although the error is not about the
			// target areas.
			target_areas[elem_index] = p_cell_population->GetCellUsingLocationIndex(elem_index)->GetCellData()->GetItem("target area");
		}
		catch (Exception&)
		{
			EXCEPTION("You need to add an AbstractTargetAreaModifier to the simulation in order to use NagaiHondaForce");
		}
	}

	// Iterate over vertices in the cell population
	for (unsigned node_index = 0; node_index < num_nodes; node_index++)
	{
		Node<DIM>* p_this_node = p_cell_population->GetNode(node_index);

		//Check if the node doesn't belong to a differentiated cell 

		if (!(std::find(mExcludedNodes.begin(), mExcludedNodes.end(), p_this_node) != mExcludedNodes.end())) {
			/* 
			 * The force on this Node is given by the gradient of the total free
			 * energy of the CellPopulation, evaluated at the position of the vertex. This
			 * free energy is the sum of the free energies of all CellPtrs in
			 * the cell population. The free energy of each CellPtr is comprised of three
			 * parts - a cell deformation energy, a membrane surface tension energy
			 * and an adhesion energy.
			 *
			 * Note that since the movement of this Node only affects the free energy
			 * of the CellPtrs containing it, we can just consider the contributions
			 * to the free energy gradient from each of these CellPtrs.
			 */
			c_vector<double, DIM> deformation_contribution = zero_vector<double>(DIM);
			c_vector<double, DIM> membrane_surface_tension_contribution = zero_vector<double>(DIM);
			c_vector<double, DIM> adhesion_contribution = zero_vector<double>(DIM);

			// Find the indices of the elements owned by this node
			std::set<unsigned> containing_elem_indices = p_cell_population->GetNode(node_index)->rGetContainingElementIndices();

			// Iterate over these elements
			for (std::set<unsigned>::iterator iter = containing_elem_indices.begin();
				iter != containing_elem_indices.end();
				++iter)
			{
				// Get this element, its index and its number of nodes
				VertexElement<DIM, DIM>* p_element = p_cell_population->GetElement(*iter);
				unsigned elem_index = p_element->GetIndex();
				unsigned num_nodes_elem = p_element->GetNumNodes();

				// Find the local index of this node in this element
				unsigned local_index = p_element->GetNodeLocalIndex(node_index);

				// Add the force contribution from this cell's deformation energy (note the minus sign)
				c_vector<double, DIM> element_area_gradient = p_cell_population->rGetMesh().GetAreaGradientOfElementAtNode(p_element, local_index);
				deformation_contribution -= 2 * GetNagaiHondaDeformationEnergyParameter()*(element_areas[elem_index] - target_areas[elem_index])*element_area_gradient;

				// Get the previous and next nodes in this element
				unsigned previous_node_local_index = (num_nodes_elem + local_index - 1) % num_nodes_elem;
				Node<DIM>* p_previous_node = p_element->GetNode(previous_node_local_index);

				unsigned next_node_local_index = (local_index + 1) % num_nodes_elem;
				Node<DIM>* p_next_node = p_element->GetNode(next_node_local_index);

				// Compute the adhesion parameter for each of these edges
				double previous_edge_adhesion_parameter = GetAdhesionParameter(p_previous_node, p_this_node, *p_cell_population);
				double next_edge_adhesion_parameter = GetAdhesionParameter(p_this_node, p_next_node, *p_cell_population);

				// Compute the gradient of each these edges, computed at the present node
				c_vector<double, DIM> previous_edge_gradient = -p_cell_population->rGetMesh().GetNextEdgeGradientOfElementAtNode(p_element, previous_node_local_index);
				c_vector<double, DIM> next_edge_gradient = p_cell_population->rGetMesh().GetNextEdgeGradientOfElementAtNode(p_element, local_index);

				// Add the force contribution from cell-cell and cell-boundary adhesion (note the minus sign)
				adhesion_contribution -= previous_edge_adhesion_parameter * previous_edge_gradient + next_edge_adhesion_parameter * next_edge_gradient;

				// Add the force contribution from this cell's membrane surface tension (note the minus sign)
				c_vector<double, DIM> element_perimeter_gradient = previous_edge_gradient + next_edge_gradient;
				double cell_target_perimeter = 2 * sqrt(M_PI*target_areas[elem_index]);
				membrane_surface_tension_contribution -= 2 * GetNagaiHondaMembraneSurfaceEnergyParameter()*(element_perimeters[elem_index] - cell_target_perimeter)*element_perimeter_gradient;
			}

			c_vector<double, DIM> force_on_node = deformation_contribution + membrane_surface_tension_contribution + adhesion_contribution;
			p_cell_population->GetNode(node_index)->AddAppliedForceContribution(force_on_node);
		
		}
	}
}

template<unsigned DIM>
double MyNagaiHondaForce<DIM>::GetNagaiHondaDeformationEnergyParameter()
{
	return NagaiHondaForce<DIM>::GetNagaiHondaDeformationEnergyParameter();
}

template<unsigned DIM>
double MyNagaiHondaForce<DIM>::GetNagaiHondaMembraneSurfaceEnergyParameter()
{
	return NagaiHondaForce<DIM>::GetNagaiHondaMembraneSurfaceEnergyParameter();
}


