#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"

#include "AbstractForce.hpp"
#include "HoneycombMeshGenerator.hpp"
#include "FixedG1GenerationalCellCycleModel.hpp"
#include "GeneralisedLinearSpringForce.hpp"
#include "OffLatticeSimulation.hpp"
#include "CellsGenerator.hpp"
#include "TransitCellProliferativeType.hpp"
#include "SmartPointers.hpp"

#include "FakePetscSetup.hpp"

#ifndef SHEARSTRESSFORCEBOTHSIDES_HPP_
#define SHEARSTRESSFORCEBOTHSIDES_HPP_


class ShearStressForceBothSides : public AbstractForce<2> {
private:

	double mStrength;
	std::vector<Node<2>*> mExcludedNodesBottom;
	std::vector<Node<2>*> mExcludedNodesTop;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractForce<2> >(*this);
		archive & mStrength;
		archive & mExcludedNodesBottom;
		archive & mExcludedNodesTop;
	}

public:
	//Default constructor
	ShearStressForceBothSides();

       	ShearStressForceBothSides(double strength, std::vector<Node<2>*> &rExcludedNodesBottom, std::vector<Node<2>*> &rExcludedNodesTop);
        
	virtual ~ShearStressForceBothSides();

	void AddForceContribution(AbstractCellPopulation<2>& rCellPopulation);

	double GetStrength();

	void SetStrength(double newStregth);
	
	void SetExcludedNodes(std::vector<Node<2>*> &rExcludedNodesBottom, std::vector<Node<2>*> &rExcludedNodesTop);

	void OutputForceParameters(out_stream& rParamsFile);
};


#include "SerializationExportWrapper.hpp"
CHASTE_CLASS_EXPORT(ShearStressForceBothSides)



#endif /*SHEARSTRESSFORCEBOTHSIDES_HPP_*/




