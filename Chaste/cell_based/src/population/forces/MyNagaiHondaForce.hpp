#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"

#include "AbstractForce.hpp"
#include "HoneycombMeshGenerator.hpp"
#include "FixedG1GenerationalCellCycleModel.hpp"
#include "GeneralisedLinearSpringForce.hpp"
#include "OffLatticeSimulation.hpp"
#include "CellsGenerator.hpp"
#include "TransitCellProliferativeType.hpp"
#include "SmartPointers.hpp"

#include "FakePetscSetup.hpp"


#include "NagaiHondaForce.hpp"

#ifndef MYNAGAIHONDAFORCE_HPP_
#define MYNAGAIHONDAFORCE_HPP_

template<unsigned DIM>
class MyNagaiHondaForce : public NagaiHondaForce<DIM>
{
	friend class TestForces;


private:
	std::vector<Node<DIM>*> mExcludedNodes;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<NagaiHondaForce>(*this);
		archive & mExcludedNodes;
	}



public:
	MyNagaiHondaForce(std::vector<Node<DIM>*> rExcludedNodes);

	void AddForceContribution(AbstractCellPopulation<DIM>& rCellPopulation);

	/**
	 * @return mNagaiHondaDeformationEnergyParameter
	 */
	double GetNagaiHondaDeformationEnergyParameter();

	/**
	 * @return mNagaiHondaMembraneSurfaceEnergyParameter
	 */
	double GetNagaiHondaMembraneSurfaceEnergyParameter();
};

#endif /*MYNAGAIHONDAFORCE_HPP_*/

