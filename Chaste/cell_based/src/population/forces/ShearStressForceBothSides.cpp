#include "ShearStressForceBothSides.hpp"




ShearStressForceBothSides::ShearStressForceBothSides()
{
}

ShearStressForceBothSides::ShearStressForceBothSides(double strength, std::vector<Node<2>*> &rExcludedNodesBottom, std::vector<Node<2>*> &rExcludedNodesTop)
	: AbstractForce<2>(),
	mStrength(strength),
	mExcludedNodesBottom(rExcludedNodesBottom),
	mExcludedNodesTop(rExcludedNodesTop)
{
	assert(mStrength >= 0.0);
	assert(mExcludedNodesBottom.size() > 0);
	assert(mExcludedNodesTop.size() > 0);
        
}


ShearStressForceBothSides::~ShearStressForceBothSides()
{
}



void ShearStressForceBothSides::AddForceContribution(AbstractCellPopulation<2>& rCellPopulation) {
	unsigned num_nodes = rCellPopulation.GetNumNodes();

	//Create a new force vectors
	c_vector<double, 2> forceUp = zero_vector<double>(2);
	forceUp(1) = mStrength;
	c_vector<double, 2> forceDown = zero_vector<double>(2);
        forceDown(1) = -mStrength;
	
	//And applies it on the nodes at the bottom or at the top of the tissue
	for (unsigned node_index=0; node_index<num_nodes; node_index++){
		Node<2>* p_this_node = rCellPopulation.GetNode(node_index);
		if(std::find(mExcludedNodesBottom.begin(),  mExcludedNodesBottom.end(), p_this_node) !=  mExcludedNodesBottom.end()){
			p_this_node->AddAppliedForceContribution(forceDown);
		}
		else if(std::find(mExcludedNodesTop.begin(),  mExcludedNodesTop.end(), p_this_node) !=  mExcludedNodesTop.end()){
			p_this_node->AddAppliedForceContribution(forceUp);	
		}
	}
}


double ShearStressForceBothSides::GetStrength() {
	return mStrength;
}

void ShearStressForceBothSides::OutputForceParameters(out_stream& rParamsFile) {
	*rParamsFile << "\t\t\t<Strength>" << mStrength << "</Strength>\n";
	AbstractForce<2>::OutputForceParameters(rParamsFile);
}

void ShearStressForceBothSides::SetStrength(double newStregth){
	mStrength = newStregth;
}


void ShearStressForceBothSides::SetExcludedNodes(std::vector<Node<2>*> &rExcludedNodesBottom, std::vector<Node<2>*> &rExcludedNodesTop){
	mExcludedNodesBottom = rExcludedNodesBottom;
	mExcludedNodesTop = rExcludedNodesTop;
}


#include "SerializationExportWrapperForCpp.hpp"
CHASTE_CLASS_EXPORT(ShearStressForceBothSides)


