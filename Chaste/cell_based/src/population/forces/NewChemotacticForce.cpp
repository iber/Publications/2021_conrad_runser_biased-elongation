/*

Copyright (c) 2005-2018, University of Oxford.
All rights reserved.

University of Oxford means the Chancellor, Masters and Scholars of the
University of Oxford, having an administrative office at Wellington
Square, Oxford OX1 2JD, UK.

This file is part of Chaste.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the University of Oxford nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "NewChemotacticForce.hpp"

#include "CellwiseDataGradient.hpp"
#include "CellLabel.hpp"

template<unsigned DIM>
NewChemotacticForce<DIM>::NewChemotacticForce()
	: AbstractForce<DIM>()
{
}

template<unsigned DIM>
NewChemotacticForce<DIM>::~NewChemotacticForce()
{
}

template<unsigned DIM>
double NewChemotacticForce<DIM>::GetNewChemotacticForceMagnitude(const double concentration, const double concentrationGradientMagnitude)
{
	return concentration; // temporary force law - can be changed to something realistic
						  // without tests failing
}

template<unsigned DIM>
void NewChemotacticForce<DIM>::AddForceContribution(AbstractCellPopulation<DIM>& rCellPopulation)
{
	std::cout << "###########################################shear stress send the force \n";
	//First we apply the force on the nodes of the bottom layer
	for (unsigned i = 0; i < mExcludedNodesBottom.size(); i++) {
		mExcludedNodesBottom[i]->AddAppliedForceContribution(forceDown);
	}
	//The we apply the force on the nodes of the top layer
	for (unsigned j = 0; j < mExcludedNodesTop.size(); j++) {
		mExcludedNodesTop[j]->AddAppliedForceContribution(forceUp);
	}
}

template<unsigned DIM>
void NewChemotacticForce<DIM>::OutputForceParameters(out_stream& rParamsFile)
{
	// No parameters to include

	// Call method on direct parent class
	AbstractForce<DIM>::OutputForceParameters(rParamsFile);
}

template<unsigned DIM>
void NewChemotacticForce<DIM>::SetStrength(double newStregth) {
	mStrength = newStregth;
}

template<unsigned DIM>
void NewChemotacticForce<DIM>::SetExcludedNodes(std::vector<Node<2>*> rExcludedNodesBottom, std::vector<Node<2>*> rExcludedNodesTop) {
	mExcludedNodesBottom = rExcludedNodesBottom;
	mExcludedNodesTop = rExcludedNodesTop;
}

// Explicit instantiation
template class NewChemotacticForce<1>;
template class NewChemotacticForce<2>;
template class NewChemotacticForce<3>;

// Serialization for Boost >= 1.36
#include "SerializationExportWrapperForCpp.hpp"
EXPORT_TEMPLATE_CLASS_SAME_DIMS(NewChemotacticForce)

