#include <cxxtest/TestSuite.h>
#include "CheckpointArchiveTypes.hpp"
#include "AbstractCellBasedTestSuite.hpp"

#include "AbstractForce.hpp"
#include "HoneycombMeshGenerator.hpp"
#include "FixedG1GenerationalCellCycleModel.hpp"
#include "GeneralisedLinearSpringForce.hpp"
#include "OffLatticeSimulation.hpp"
#include "CellsGenerator.hpp"
#include "TransitCellProliferativeType.hpp"
#include "SmartPointers.hpp"

#include "FakePetscSetup.hpp"

#ifndef SHEARSTRESSFORCE_HPP_
#define SHEARSTRESSFORCE_HPP_


class ShearStressForce : public AbstractForce<2>{
private:

	double mStrength;
	std::vector<Node<2>*> mExcludedNodes;


	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & archive, const unsigned int version)
	{
		archive & boost::serialization::base_object<AbstractForce<2> >(*this);
		archive & mStrength;
		archive & mExcludedNodes;
	}

public:
	ShearStressForce(double strength, std::vector<Node<2>*> rExcludedNodes);

	void AddForceContribution(AbstractCellPopulation<2>& rCellPopulation);

	double GetStrength();

	void OutputForceParameters(out_stream& rParamsFile);
};

#endif /*SHEARSTRESSFORCE_HPP_*/
