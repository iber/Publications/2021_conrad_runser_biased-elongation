
#include "ShearStressForce.hpp"



ShearStressForce::ShearStressForce(double strength, std::vector<Node<2>*> rExcludedNodes)
		:AbstractForce<2>(),
		mStrength(strength),
		mExcludedNodes(rExcludedNodes)
	{
		assert(mStrength >= 0.0);
		assert(mExcludedNodes.size() > 0);
	}

	void ShearStressForce::AddForceContribution(AbstractCellPopulation<2>& rCellPopulation){
		c_vector<double, 2> force = zero_vector<double>(2);
		force(1) = mStrength;

		//First we apply the Y force to all the nodes 
		for (unsigned node_index = 0; node_index < rCellPopulation.GetNumNodes(); node_index++){
			rCellPopulation.GetNode(node_index)->AddAppliedForceContribution(force);
		}

		//Second, we remove the applied forces on the excluded nodes owned by the differentiated cells
		for (unsigned i = 0; i < mExcludedNodes.size(); i++) {
			mExcludedNodes[i]->ClearAppliedForce();
		}
	}

	double ShearStressForce::GetStrength(){
		return mStrength;
	}

	void ShearStressForce::OutputForceParameters(out_stream& rParamsFile){
		*rParamsFile << "\t\t\t<Strength>" << mStrength << "</Strength>\n";
		AbstractForce<2>::OutputForceParameters(rParamsFile);
	}

