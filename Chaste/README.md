The code provided in this folder modifies and extends the cell-based model included in Chaste, release 2018.1, developed at University of Oxford.

To download and install Chaste please follow the instructions given on the Chaste webpage (https://www.cs.ox.ac.uk/chaste/).

After the installation, merge the content of the cell_based folder given in this repository with the original Chaste cell_based folder, overwriting existing files.

Run the following command to start a lung elongation simulation:

scons cell_based/test/tutorial/INPUT_stress_1.5_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp


Modified files:

* cell_based/src/cell/Cell.cpp
* cell_based/src/cell/Cell.hpp
* cell_based/src/population/VertexBasedCellPopulation.cpp
* cell_based/src/population/VertexBasedCellPopulation.hpp
* cell_based/src/simulation/AbstractCellBasedSimulation.cpp
* cell_based/src/simulation/OffLatticeSimulation.cpp 
* cell_based/src/simulation/modifiers/SimpleTargetAreaModifier.cpp
* cell_based/src/simulation/numerical_methods/AbstractNumericalMethod.cpp 
* cell_based/src/simulation/numerical_methods/ForwardEulerNumericalMethod.cpp
* cell_based/src/writers/population_writers/BoundaryNodeWriter.cpp
* cell_based/src/writers/population_writers/BoundaryNodeWriter.hpp
* cell_based/src/writers/population_writers/CellPopulationElementWriter.cpp
* cell_based/src/writers/population_writers/NodeLocationWriter.cpp


Added files:

* cell_based/src/cell/cycle/myCycle.cpp
* cell_based/src/cell/cycle/myCycle.hpp
* cell_based/src/cell/properties/SideCellProperties.cpp
* cell_based/src/cell/properties/SideCellProperties.hpp
* cell_based/src/population/forces/MyNagaiHondaForce.cpp
* cell_based/src/population/forces/MyNagaiHondaForce.hpp
* cell_based/src/population/forces/NewChemotacticForce.cpp
* cell_based/src/population/forces/NewChemotacticForce.hpp
* cell_based/src/population/forces/ShearStressForce.cpp
* cell_based/src/population/forces/ShearStressForce.hpp
* cell_based/src/population/forces/ShearStressForceBothSides.cpp
* cell_based/src/population/forces/ShearStressForceBothSides.hpp
* cell_based/src/writers/population_writers/CellLewisLawWriter.cpp
* cell_based/src/writers/population_writers/CellLewisLawWriter.hpp
* cell_based/src/writers/population_writers/CellVolumesWriter.cpp
* cell_based/src/writers/population_writers/CellVolumesWriter.hpp
* cell_based/src/writers/population_writers/MyBoundaryNodeWriter.cpp
* cell_based/src/writers/population_writers/MyBoundaryNodeWriter.hpp
* cell_based/test/tutorial/INPUT_stress_0.0_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_0.0_line_15.0_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_1.0_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_1.1_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_1.25_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_1.5_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
* cell_based/test/tutorial/INPUT_stress_2.0_line_0.5_surface_10.0_area_def_100.0_division_time_12.0.hpp
