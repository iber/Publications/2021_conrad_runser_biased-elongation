This folder contains the model files for COMSOL Multiphysics (version 5.4) to run the fluid flow simulations. The following modules are required:

* CFD Module
* CAD Import Module

Each file contains the geometry for a specific value of the different lumen semi-axis, as indicated in the file names.
